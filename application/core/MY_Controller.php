<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of MY_Controller
 *
 * @author tonylee
 */
class MY_Controller extends CI_Controller {
                
    public function __construct() { 
        
        parent::__construct();
    }
}



class MY_Admin_Class extends MY_Controller{
	
    public $base_allow_uris = array();
	 
    
    public function __construct() {
        
        parent::__construct();
       
        # initial 
        $this->data['meta_title'] = config_item('ams_title');
		
	# load helper
        $this->load->helper('form');
	$this->load->helper('ams');
        $this->load->helper('url');
		
	# load library
        $this->load->library('form_validation');
        $this->load->library('session');
		
	#load model
        $this->load->model('m_user');        
        $this->load->model('m_admin'); 
    }
}


class MY_Public_Class extends MY_Controller{
    
    public function __construct() {
        
        parent::__construct();
        
        # load library
        $this->load->library('User_Attempt');
    }
}

class MY_Bus_Service_Class extends MY_Controller{
    
    public function __construct() {
        
        parent::__construct();

        // load model
        $this->load->model('m_admin'); 
    }   
    
}
