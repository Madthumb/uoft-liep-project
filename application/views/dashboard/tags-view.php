<?php $sp = 0;  foreach ($result as $key => $value ): ?>
<?php if( $sp%2 == 0 ) : ?>
<div class="clearfix breadcrumb"></div>
<?php endif; ?>
<div class="panel-group col-xs-12 col-sm-6 dashboard-panel">
        <div class="panel panel-primary live-stream">
                <div class="panel-heading">
                        <h4 class="panel-title">
                                <a href="#total-participants" data-toggle="collapse">
                                        <span class="glyphicon glyphicon-calendar selected"></span>
                                        <span class="glyphicon glyphicon-th-list <?php
                                            if( !isset($_POST['options']) or is_numeric($_POST['options']) ){
                                                echo 'selected';
                                            }
                                        ?>"></span>
                                        <span class="glyphicon glyphicon-th <?php
                                            if( isset($_POST['options']) and !is_numeric($_POST['options']) ){
                                                echo 'selected';
                                            }
                                        ?>"></span>    
                                        <?php echo $value->name; ?> Tags Search Count</a>
                                        <?php  show_result();   ?>
                        </h4>
                </div>
                <div class="panel-collapse in" id="total-participants">
                        <div class="panel-body">    
                                <table class="table table-condensed">
                                        <thead>
                                                <tr>
                                                        <th class="btn-sm">Tags</th>
                                                        <th class="btn-sm text-center">Counts</th>
                                                </tr>
                                        </thead>
                                        ‌<tbody>
                                                <?php if(isset($value->data) and !empty($value->data)): ?>   
                                                <?php foreach ($value->data as $v) : ?>
                                                <tr>
                                                        <td class="btn-sm">
                                                                <?php echo current($v);?>
                                                        </td>
                                                        <td class="text-center btn-sm">
                                                                <?php echo key($v); ?>
                                                        </td>
                                                </tr>                        
                                                <?php endforeach; ?>
                                                <?php else: ?>
                                                        <tr>
                                                                <td class="btn-sm" colspan="2">
                                                                        There is no records available.
                                                                </td>
                                                        </tr>
                                                <?php endif;?>                                                
                                        </tbody>
                                </table>
                        </div>  
                </div>            
        </div>
</div>
<?php $sp++; endforeach; ?>