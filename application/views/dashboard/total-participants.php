<div class="panel-group col-xs-12 col-sm-6 dashboard-panel">

        <div class="panel panel-primary live-stream">
                <div class="panel-heading">
                        <h4 class="panel-title">
                                <a href="#total-participants" data-toggle="collapse">
                                        <span class="glyphicon glyphicon-th selected"></span>                                    
                                        Total Participants</a>
                        </h4>
                </div>
                <div class="panel-collapse in" id="total-participants">
                        <div class="panel-body">    
                                <table class="table table-condensed">
                                        <thead>
                                                <tr>
                                                        <th class="btn-sm">Streams</th>
                                                        <th class="btn-sm text-center">Participants</th>
                                                </tr>
                                        </thead>
                                        ‌<tbody>
                                        <?php if(!empty($result)): ?>    
                                        <?php foreach ($result as $key => $value) : ?>
                                                <tr>
                                                        <td class="btn-sm">
                                                                <?php echo $value->name; ?>
                                                        </td>
                                                        <td class="text-center btn-sm">
                                                                <?php echo $value->num; ?>
                                                        </td>
                                                </tr>                        
                                        <?php endforeach; ?>
                                        <?php else: ?>
                                                <tr>
                                                        <td class="btn-sm" colspan="2">
                                                                There is no records available.
                                                        </td>
                                                </tr>
                                        <?php endif;?>
                                                
                                        </tbody>
                                </table>
                        </div>  
                </div>            
        </div>
</div>