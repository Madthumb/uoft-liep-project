<div class="clearfix breadcrumb"></div>
<div class="panel-group col-xs-12 dashboard-panel">
        <div class="panel panel-primary live-stream">
                <div class="panel-heading">
                        <h4 class="panel-title">
                                <a href="#total-participants" data-toggle="collapse">
                                        <span class="glyphicon glyphicon-calendar selected"></span>
                                        <span class="glyphicon glyphicon-th-list <?php
                                            if( !isset($_POST['options']) or is_numeric($_POST['options']) ){
                                                echo 'selected';
                                            }
                                        ?>"></span>
                                        <span class="glyphicon glyphicon-th <?php
                                            if( isset($_POST['options']) and !is_numeric($_POST['options']) ){
                                                echo 'selected';
                                            }
                                        ?>"></span>      
                                        Participants </a>
                                        <?php  show_result();   ?>
                        </h4>
                </div>
                <div class="panel-collapse in" id="total-participants">
                        <div class="panel-body">    
                                ‌<div class="table-responsive" id="participant-list">      
                                <table class="table table-condensed" id="myTable">
                                        <thead>
                                                <tr>
                                                        <th class="btn-sm">Name</th>
                                                        <th class="btn-sm">Stream</th>
                                                        <th class="btn-sm text-center">View</th>
                                                        <th class="btn-sm text-center">Share</th>
                                                        <th class="btn-sm text-center">Print</th>
                                                        <th class="btn-sm text-center">Search</th>
                                                        <th class="btn-sm text-center">Download</th>
                                                        <th class="btn-sm text-center">Action</th>
                                                </tr>
                                        </thead>
                                        ‌<tbody class="list">
                                        <?php if(!empty($result)): ?>                                              
                                        <?php foreach ($result as $key => $value) : ?>
                                                <tr>
                                                        <td class="btn-sm username">
                                                                <?php echo $value->user_name; ?>
                                                        </td>
                                                        <td class="btn-sm streamname">
                                                                <?php echo $value->stream_name; ?>
                                                        </td>
                                                        <td class="text-center btn-sm view">
                                                                <?php echo $value->view; ?>
                                                        </td>
                                                        <td class="text-center btn-sm share">
                                                                <?php echo $value->share; ?>
                                                        </td>
                                                        <td class="text-center btn-sm print">
                                                                <?php echo $value->print; ?>
                                                        </td>
                                                        <td class="text-center btn-sm search">
                                                                <?php echo $value->search; ?>
                                                        </td>
                                                        <td class="text-center btn-sm download">
                                                                <?php echo $value->download; ?>
                                                        </td>
                                                        <td class="text-center btn-sm option_id">
                                                                <?php echo $value->option_id; ?>
                                                        </td>                                                        
                                                </tr>                        
                                        <?php endforeach; ?>
                                        <?php else: ?>
                                                <tr>
                                                        <td class="btn-sm" colspan="8">
                                                                There is no records available.
                                                        </td>
                                                </tr>
                                        <?php endif;?>                                                  
                                        </tbody>
                                </table>
                                <div id="pager" class="pager">
                                        <form>
                                                <span class="glyphicon glyphicon-fast-backward first"></span>
                                                <span class="glyphicon glyphicon-backward prev"></span>
                                                <input type="text" class="pagedisplay"/>
                                                <span class="glyphicon glyphicon-forward next"></span>
                                                <span class="glyphicon glyphicon-fast-forward last"></span>
                                                
                                                <select class="pagesize">
                                                        <option selected="selected"  value="10">10</option>
                                                        <option value="20">20</option>
                                                        <option value="30">30</option>
                                                </select>
                                        </form>
                                </div>                                    
                                </div>    
                        </div>  
                </div>            
        </div>
</div>