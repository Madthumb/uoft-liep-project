<div class="skills-tip">
    * Any change to the Skills Tag(s) below will affect to all the related participant record. Please be careful.
</div>
<div id="skills" class="panel-group">
<?php
$item = 0;
foreach ($skills as $key => $value) {
    
    if( is_string($key) )
    {
?>
        <div class="panel panel-primary live-stream">
            <div class="panel-heading">
              <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#skills" href="#row-<?php echo str_replace(' ', '-', strtolower($key)); ?>">Stream: <?php echo $key; ?></a>
              </h4>
            </div>
            
            <div id="row-<?php echo str_replace(' ', '-', strtolower($key)); ?>" class="panel-collapse collapse <?php  echo ($counter == 0 ? 'in' : ''); ?>">
                
                <div class="panel-body">
                                   
                        
                    <?php 
                    
                    
                    $ttl      = count($value);
                    $splitter = round($ttl / 2);
                    $counter = 1;
                    foreach ($value as $v) {
                       if( $counter == 1 or $counter == ($splitter+1) )  
                          echo '<div class="col-md-6">'; 
                    ?>      
                        <!-- skills tag -->           
                        <div role="<?php echo str_replace(' ', '_', $v); ?>" id="wraper-<?php echo $item; ?>" class="col-md-11 skills-wrapper">
                        <div class="input-group">
                                <input type="text" id="skills-<?php echo $item; ?>" readonly="" class="form-control" value="<?php echo $v; ?>">
                                <div class="input-group-btn">
                                    <button data-toggle="dropdown" class="btn btn-default dropdown-toggle" type="button">Action <span class="caret"></span></button>          
                                    <ul class="dropdown-menu pull-right">
                                            <li><a class="edit" id="edit-<?php echo $item; ?>">Edit</a></li>
                                            <li><a class="save" id="save-<?php echo $item; ?>">Save</a></li>

                                            <li class="divider"></li>
                                            <li><a class="del" id="del-<?php echo $item; ?>">Delete</a></li>
                                    </ul>
                                </div>
                        </div>
                        </div>     
                    
                    <!-- end skills tag -->                
                    <?php     
                    
                      if( $counter == $splitter or $counter == $ttl ){  
                          echo '</div>';                    
                      }
                        $counter++;  
                        $item++;
                    } ?>    
                    
                    
                </div>
            </div>
        </div>


<?php
    }
}
?>
</div>
