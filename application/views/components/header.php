<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="title" content="<?php echo isset($meta_title) ? $meta_title : ''; ?>" />
    <meta name="description" content="LIEP" />
    <meta name="keywords" content="LIEP"/>
    <meta name="author" content="Tony Lee <var.tonylee@gmail.com>">
    <meta property="og:title" content="<?php echo isset($meta_title) ? $meta_title : ''; ?>" /> 
    <meta property="og:description" content="LIEP" />
    <meta property="og:url" content="<?php echo base_url(); ?>" />
    <meta property="og:type" content="website" />

    <title><?php echo isset($title) ? $title : 'LIEP'; ?></title>

    <link href="<?php echo config_item('app_url');?>resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo config_item('app_url');?>resources/css/spacelab.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo config_item('app_url');?>resources/css/datepicker.css" rel="stylesheet">    
    <link href="<?php echo config_item('app_url');?>resources/css/liep.css" rel="stylesheet">
    
    <!--[if IE 8]>  
    <link href="<?php echo config_item('app_url');?>resources/css/ie8.css" rel="stylesheet">
    <![endif]-->  

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <?php
        if(strpos($_SERVER["REQUEST_URI"], 'participant') !== false 
                or strpos($_SERVER["REQUEST_URI"], 'dashboard') !== false )
           echo '<script src="'.config_item('app_url').'resources/js/lib/list.js"></script>';     
    ?>
    
    <script data-main="<?php echo config_item('app_url');?>resources/js/include" src="<?php echo config_item('app_url');?>resources/js/lib/require.min-2.1.8.js"></script>
    
    <script>var baseLink = "<?php echo config_item('app_url');?>";</script>

  </head>

  <body>
    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="dashboard.html"><?php echo config_item('ams_title'); ?></a>
        </div>          
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <?php
                foreach ( config_item('menu') as $k=>$v ){                  
                  $ID   = str_replace(' ', '-', strtolower($v));                   
                  $class = '';
                  if( $k === $active_menu_index )
                    $class = 'class="active"';
                  
                  echo '<li id="'.$ID.'" '.$class.'><a href="'.config_item('app_url').$ID.'.html">'.$v.'</a></li>';
                }
            ?>
          </ul>
          <ul class="nav navbar-nav navbar-right">                             
            <li class="stick username">
                <a><?php echo 'Hi '. $this->session->userdata['name']; ?></a>
            </li>  
            <li class="stick logout">
                <a href="logout.html">Logout</a>
            </li>                
          </ul>
        </div>
      </div>
    </div>    
     <div class="container"> 
    <div class="breadcrumb"></div>          