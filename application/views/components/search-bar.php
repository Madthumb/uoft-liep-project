<div class="navbar navbar-default dashboard-panel dashboard-search-bar">
        <form method="post" action="">
                <ul class="nav nav-pills ">
                        <li class="date-picker"><a>
                                <span class="glyphicon glyphicon-calendar selected"></span>
                                Start: <input type="text" class="span2 date-picker" value="<?php isset_value($_POST['start-date']); ?>" name="start-date" id="dpd1"></a></li>
                        <li class="date-picker"><a>
                                <span class="glyphicon glyphicon-calendar selected"></span>
                                End: <input type="text" class="span2 date-picker" value="<?php isset_value($_POST['end-date']); ?>" name="end-date" id="dpd2"></a></li>
                        <li class="auth-button">
                                <div class="btn-group" data-toggle="buttons">
                                        <label class="btn btn-sm btn-default active">
                                            <input type="radio" name="options" value="10" checked> 
                                                <span class="glyphicon glyphicon-th-list"></span>
                                                Top 10 records 
                                        </label>
                                        <label class="btn btn-sm btn-default">
                                            <input type="radio" name="options" value="all"> 
                                                <span class="glyphicon glyphicon-th"></span>
                                                All records
                                        </label>
                                </div>          
                        </li>

                        <li class="auth-button"><button id="authorize-button" class="btn btn-sm btn-default" type="submit">Submit</button></li>
                </ul> 
        </form>    
</div>