<?php
if( empty($streams) )
    echo '<div class="skills-tip">* Please enter a Stream option first.</div>';

?>
<div class="add-new-form">
    <form class="form-horizontal" role="form">
        <div class="panel panel-primary">
            <div class="panel-heading">
              <h5 class="panel-title">Manage Stream</h5>
            </div>
            <div class="panel-body">  
                <div class="form-group col-sm-11 add-stream">
                    <div class="input-group col-sm-5">
                    <input type="text" class="form-control" id="addStream" placeholder="Enter a steam name." >
                    <span class="input-group-btn">
                        <button class="btn btn-default" id="addStream-btn" type="button">Add Stream</button>
                    </span>
                    </div>               
                </div>

                <div class="row">
                    <div id="add-stream-result" class="well col-xs-10 col-xs-offset-1">
                        <div class="row"><?php
                         
                        if( isset($streams) )
                        {
                            for($i = 0; $i < count($streams); $i++){
                            
                               echo '<div class="col-xs-6 stream-wrapper" id="wraper-'.$i.'" role="'.$streams[$i]->id.'">'.
                                        '<div class="input-group">'.
                                          '<input value="'.$streams[$i]->name.'" type="text" class="form-control" readonly="" id="stream-'.$i.'">'.
                                          '<div class="input-group-btn">'.
                                            '<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">Action <span class="caret"></span></button>'.
                                            '<ul class="dropdown-menu pull-right">'.
                                              '<li><a id="edit-'.$i.'" class="edit">Edit</a></li>'.
                                              '<li><a id="save-'.$i.'" class="save">Save</a></li>'.
                                              '<li class="divider"></li>'.
                                              '<li><a id="del-'.$i.'" class="del">Delete</a></li>'.
                                            '</ul>'.
                                          '</div>'.
                                        '</div>'.
                                      '</div>';                                

                                
                            }
                        }
                        ?></div><!-- /.row -->
                    </div> 
                </div>             
            </div>
        </div>        
    </form>
</div>


                          