<div class="sub-menu">
    <ul style="margin-bottom: 15px;" id="myTab" class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#add-mew" id="add-mew-link">Add/Edit</a></li>
            <li class=""><a data-toggle="tab" href="#details">Details</a></li>
    </ul>
</div>
<div id="myTabContent" class="tab-content">
    <div id="add-mew" class="tab-pane fade active in"><?php $this->load->view('participants/form', $subview_data);  ?></div>
    <div id="details" class="tab-pane fade"><?php $this->load->view('participants/lists', $subview_data); ?></div>
</div>
