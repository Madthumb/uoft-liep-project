<!DOCTYPE html>
<html lang="en">
  <head>
    <style>
      body{
        font-family: Arial, Helvetica, sans-serif;        
      }    
      table{
        border: thin solid black;
        width: 100%;
        font-size: 13px;
      }
      td.name{
        font-weight: bold;  
      }
      th.short{
        width: 30%;
        text-align: left;
        height: 30px;
      }
      .bold{
        font-weight: bold;
        text-align: left;
      }
      h3{
        font-size: 21px;
        font-weight: bold;
      }
      .footer{
        margin-top: 50px; 
        border: none;
        font-size: 10px;
      }
    </style>
  </head>
  <body>
    <h3>Licensing International Engineers into the Profession (LIEP) Program</h3>
    <table>
      <tbody>
        <tr>
          <th class="short">Participant ID:</th>
          <td><?php echo $data->p_id; ?></td>
        </tr>
        <tr>
          <th class="short">Stream:</th>
          <td><?php echo $data->stream_id; ?></td>
        </tr>
        <tr>
          <th class="short">Full Name:</th>
          <td class="name"><?php echo dis_strim( $data->full_name, ' ' ); ?></td>
        </tr>
      </tbody>
    </table>
    <br>
    <table>
      <tbody>
        <tr>
          <th class="bold">Education/Degree:</th>
        </tr>
        <tr>
          <td><?php echo dis_strim( $data->edu_info, ',' ); ?></td>
        </tr>
      </tbody>
    </table>    
    <br>
    <table>
      <tbody>
        <tr>
          <th class="short">Years of Experience:</th>
          <td><?php echo $data->years_of_experience; ?></td>
        </tr>
      </tbody>
    </table>    
    <br>
    <table>
      <tbody>
        <tr>
          <th class="bold">Skills and Expertise:</th>
        </tr>
        <tr>
          <td><?php
					
                echo dis_strim( $data->skills );
                
                ?></td>
        </tr>
      </tbody>
    </table>    
    <br>
    <table>
      <tbody>
        <tr>
          <th class="bold">Additional Qualifications:</th>
        </tr>
        <tr>
          <td><?php echo dis_strim( $data->aq_info ); ?></td>
        </tr>
      </tbody>
    </table>
    <br>
    <table>
      <tbody>
        <tr>
          <th class="bold">Profession Certifications/Associations:</th>
        </tr>
        <tr>
          <td><?php echo dis_strim( $data->cer_info, '<br>' ); ?></td>
        </tr>
      </tbody>
    </table>
    <br>
    <table>
      <tbody>
        <tr>
          <th class="bold">Other Information:</th>
        </tr>
        <tr>
          <td><?php echo $data->other_info; ?></td>
        </tr>
      </tbody>
    </table>
    <br>
    <table>
      <tbody>
        <tr>
          <th class="bold">LIEP Program Completion:</th>
        </tr>
        <tr>
          <td>
            <?php 
                echo date('F Y', $data->liep_completion); 

                if( (int)$data->liep_completion < (int)time()  ){
                    echo ' (Successfully Completed)';
				}
                else{
                    echo ' (Expected)';
				}
            ?> 
          </td>
        </tr>
      </tbody>
    </table>
    <table class="footer">
      <tbody>
        <tr>
          <th class="bold">Amena Zafar, LIEP Work Experence and Mentoring Coordinator </th>
        </tr>
        <tr>
          <td>
            Phone: 416-987-5662
            <br>
            Email: scs.liep@utoronto.ca
          </td>
        </tr>
      </tbody>
    </table>    
  </body>
</html>