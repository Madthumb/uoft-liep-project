<div class="row">
    
    <div class="col-sm-8 text-center">
        <div class="skills-tip">Participant items added to page will not be stored until the Save button is clicked.</div>
    </div>    
    <div class="col-sm-3 pull-right">
        <input class="btn btn-default form-control" role="add-new-participant" type="button"  value="<?php
                    if(isset($the_one) )
                        echo 'Update';
                    else 
                        echo 'Save';                
                    ?>">
    </div>
</div>    
<br>
<div class="add-new-form">
    <form class="form-horizontal" role="form">
    <div class="form-group">
      <label for="participantID" class="col-sm-2 control-label">Participant ID:</label>
      <div class="col-sm-2">
          <input type="text" class="form-control" id="participantID" placeholder="Enter a participant ID" value="<?php echo (isset($the_one) ? $the_one->p_id : ''); ?>">
      </div>  

      <label for="stream" class="col-sm-2 control-label">Stream:</label>
      <div class="col-sm-2">
        <select class="form-control" id="stream-id"><?php 
        if(isset($author) ){
            foreach ($steams as $key => $value) {
                $selected = '';
                if(isset($the_one) ){
                    if( $steams[$key]->id == $the_one->stream_id ) 
                        $selected = 'selected';
                }
                    
                echo '<option '.$selected.' value="'.$steams[$key]->id.'">'.$steams[$key]->name.'</option>';
                
            }
        }
              
            
  ?></select>        
      </div>
      
      <label for="ep-years" class="col-sm-2 control-label">Experience:</label>
      <div class="col-sm-2">
        <select class="form-control" id="ep-years">
<?php 
        if(isset($author) ){
            foreach ($ex_range as $key => $value) {
                $selected = '';
                if(isset($the_one) ){
                    if( $key == $the_one->years_of_experience ) 
                        $selected = 'selected';
                }
                    
                echo '<option '.$selected.' value="'.$key.'">'.$value.'</option>';
                
            }
        }
              
            
  ?>
        </select>
      </div>
      
    </div>   
<?php
if(isset($the_one) ){
    list($fname, $mname, $lname)  = explode('|', $the_one->full_name);
}
?>
    <div class="form-group">
        <label for="first-name" class="col-sm-2 control-label">First Name:</label>         
        <div class="col-sm-2">
            <input type="text" class="form-control" id="first-name" value="<?php echo (isset($the_one) ? $fname : ''); ?>">
        </div>  
      
        <label for="middle-name" class="col-sm-2 control-label">Middle Name:</label>         
        <div class="col-sm-2">
            <input type="text" class="form-control" id="middle-name" value="<?php echo (isset($the_one) ? $mname : ''); ?>">
        </div>  
        
        <label for="last-name" class="col-sm-2 control-label">Last Name:</label>         
        <div class="col-sm-2">
            <input type="text" class="form-control" id="last-name" value="<?php echo (isset($the_one) ? $lname : ''); ?>">
        </div>          
        
    </div>
        
    <div class="panel panel-primary">
        <div class="panel-heading">
          <h5 class="panel-title">Skills and Expertise</h5>
        </div>
        <div class="panel-body">  
            <div class="form-group col-sm-11">
                <div class="input-group col-sm-5">
                <input type="text" class="form-control" id="addSkill" placeholder="Skill key word only" >
                <span class="input-group-btn">
                    <button class="btn btn-default" id="addSkill-btn" type="button">Add Skill</button>
                </span>
                </div>
            </div>
            <div class="row">
                <div id="add-skills-result" class="well col-xs-10 col-xs-offset-1"><?php
                if(isset($the_one) and !empty($the_one->skills)){
                    $skills = explode('|', $the_one->skills);
                    
                    $i = 0;
                    $prefix = 'se-';
                    foreach ($skills as $value) {
                        if(is_numeric($value)){
                            break;
                        }
                ?>
                    <div class="btn-group" id="<?php echo $prefix.$i; ?>">
                        <button class="tags edit skills-tag btn btn-success  btn-xs" type="button" role="#<?php echo $prefix.$i; ?>">
                            <span class="tag-label"><?php echo str_replace('[,]', '', $value); ?></span>
                        </button>
                        <div class="btn-group">
                            <button data-toggle="dropdown" class="btn btn-default dropdown-toggle tag-action" type="button"><span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <?php
                                    /*
                                    <li><a class="tag-edit" target="#addSkill" role="#<?php echo $prefix.$i; ?>">Edit</a></li>
                                     * 
                                     */
                                ?>
                                <li><a class="tag-del" target="#add-skills-result" role="#<?php echo $prefix.$i; ?>">Delete</a></li>
                            </ul>
                    </div>                            
                    </div>    

               <?php $i++; } }
                ?></div> 
            </div>             
        </div> 
    </div>

    
    <div class="panel panel-primary">
        <div class="panel-heading">
          <h5 class="panel-title">Education</h5>
        </div>
        <div class="panel-body">            
           <div class="form-group col-sm-11">

                <label class="col-sm-2 control-label">Degree Name<span class="mandatory">*</span>:</label>          
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="edu" placeholder="Enter a degree">
                </div>  

           </div>
           <div class="form-group col-sm-11">         
                <label for="middle-name" class="col-sm-2 control-label">Year:</label>             
                <div class="col-sm-3">
                    <input type="text" class="form-control" id="edu-year" placeholder="Enter a year">
                </div>  

                <label for="last-name" class="col-sm-2 control-label">Country:</label>            
                <div class="col-sm-5 countries">
                    <input type="text" class="form-control typeahead" id="edu-country" placeholder="Enter a country name">
                </div>
           </div>
           <div class="form-group col-sm-12">     
               <button class="btn btn-default col-sm-2 pull-right" id="edu-btn" type="button">Add Item </button>
           </div>      
        </div> 
        <div class="row">
            <div id="add-edu-result" class="well col-xs-10 col-xs-offset-1"><?php
                if(isset($the_one)){
                    $edu_info = explode('|', $the_one->edu_info);
                    
                    $i = 0;
                    $prefix = 'edu-';                    
                    foreach ($edu_info as $value) {                    
                ?>
                    <div class="btn-group" id="<?php echo $prefix.$i; ?>">
                    <button class="tags edu-tag btn btn-success btn-xs" type="button" role="#<?php echo $prefix.$i; ?>">
                        <span class="tag-label"><?php echo str_replace('[,]', '', $value); ?></span>
                    </button>
                    <div class="btn-group">
                        <button data-toggle="dropdown" class="btn btn-default dropdown-toggle tag-action" type="button"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a class="tag-edit" target="#edu" role="#<?php echo $prefix.$i; ?>">Edit</a></li>
                            <li><a class="tag-del" target="#add-edu-result" role="#<?php echo $prefix.$i; ?>">Delete</a></li>
                        </ul>
                    </div>  
                    </div> 
               <?php $i++; } }
                ?></div> 
        </div>
    </div>

 
    <div class="panel panel-primary">
        <div class="panel-heading">
          <h5 class="panel-title">Additional Qualifications</h5>
        </div>
        <div class="panel-body">            
           <div class="form-group col-sm-11">
                <label class="col-sm-2 control-label">Qualification Name<span class="mandatory">*</span>:</label>          
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="aq" placeholder="Enter a qualification">
                </div>  

           </div>
           <div class="form-group col-sm-11">         
                <label for="middle-name" class="col-sm-2 control-label">Year:</label>             
                <div class="col-sm-3">
                    <input type="text" class="form-control" id="aq-year" placeholder="Enter a year">
                </div>  

                <label for="last-name" class="col-sm-2 control-label">Country:</label>            
                <div class="col-sm-5 countries">
                    <input type="text" class="form-control typeahead" id="aq-country" placeholder="Enter a country name">
                </div>
           </div>
           <div class="form-group col-sm-12">     
               <button class="btn btn-default col-sm-2 pull-right"  id="aq-btn" type="button">Add Item </button>
           </div>      
        </div> 
        <div class="row">
            <div id="add-aq-result" class="well col-xs-10 col-xs-offset-1"><?php
                if(isset($the_one)){
                    $aq_info = explode('|', $the_one->aq_info);

                    $i = 0;
                    $prefix = 'aq-';                      
                    foreach ($aq_info as $value) {  
                ?>
                    <div class="btn-group" id="<?php echo $prefix.$i; ?>">
                    <button class="tags aq-tag btn btn-success btn-xs" type="button" role="#<?php echo $prefix.$i; ?>">
                        <span class="tag-label"><?php echo str_replace('[,]', '', $value); ?></span>
                    </button>
                    <div class="btn-group">
                        <button data-toggle="dropdown" class="btn btn-default dropdown-toggle tag-action" type="button"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a class="tag-edit" target="#aq" role="#<?php echo $prefix.$i; ?>">Edit</a></li>
                            <li><a class="tag-del" target="#add-aq-result" role="#<?php echo $prefix.$i; ?>">Delete</a></li>
                        </ul>
                    </div>                          
                    </div>    
               <?php $i++; } }
                ?></div>
        </div>        
    </div>        
    
        
    
    <div class="panel panel-primary">
        <div class="panel-heading">
          <h5 class="panel-title">Professional Certifications/Associations</h5>
        </div>
        <div class="panel-body">            
           <div class="form-group col-sm-11">

                <label class="col-sm-2 control-label">Certification/ Association Name<span class="mandatory">*</span>:</label>          
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="cer" placeholder="Enter a qualification">
                </div>  

           </div>
           <div class="form-group col-sm-11">         
                <label for="middle-name" class="col-sm-2 control-label">Year:</label>             
                <div class="col-sm-3">
                    <input type="text" class="form-control" id="cer-year" placeholder="Enter a year">
                </div>  

                <label for="last-name" class="col-sm-2 control-label">Country:</label>            
                <div class="col-sm-5 countries">
                    <input type="text" class="form-control typeahead" id="cer-country" placeholder="Enter a country name">
                </div>
           </div>
           <div class="form-group col-sm-12">     
               <button class="btn btn-default col-sm-2 pull-right"  id="cer-btn" type="button">Add Item </button>
           </div>      
        </div> 
        <div class="row">
            <div id="add-cer-result" class="well col-xs-10 col-xs-offset-1"><?php
                if(isset($the_one)){
                    $cer_info = explode('|', $the_one->cer_info);

                    $i = 0;
                    $prefix = 'cer-';                     
                    foreach ($cer_info as $value) {                    
                ?>
                    <div class="btn-group" id="<?php echo $prefix.$i; ?>">
                    <button class="tags cer-tag btn btn-success btn-xs" type="button" role="#<?php echo $prefix.$i; ?>">
                        <span class="tag-label"><?php echo str_replace('[,]', '', $value); ?></span>
                    </button>
                        <div class="btn-group">
                        <button data-toggle="dropdown" class="btn btn-default dropdown-toggle tag-action" type="button"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a class="tag-edit" target="#cer" role="#<?php echo $prefix.$i; ?>">Edit</a></li>
                            <li><a class="tag-del" target="#add-cer-result" role="#<?php echo $prefix.$i; ?>">Delete</a></li>
                        </ul>                        
                    </div>                          
                    </div>                          
               <?php $i++; } }
                ?></div> 
        </div>        
    </div>
        
        
    <div class="panel panel-primary">
        <div class="panel-heading">
          <h5 class="panel-title">Other information</h5>
        </div>
        <div class="panel-body">            
           <div class="form-group col-sm-12">
             <textarea class="form-control" id="other-info" rows="3"><?php echo (isset($the_one) ? $the_one->other_info : ''); ?></textarea>
           </div>   
        </div> 
    </div>        
        
        
    <div class="panel panel-primary">
        <div class="panel-heading">
          <h5 class="panel-title">Search Meta Tag</h5>
        </div>
        <div class="panel-body">            
           <div class="form-group col-sm-12">
             <textarea class="form-control" id="search-tag" rows="4"><?php 
 
             if(isset($the_one)){
                 echo $the_one->search_meta;
                 
                 
             }
             ?></textarea>
           </div>   
        </div> 
    </div>    


      <div class="form-group">
        <label class="col-sm-1 control-label text-left" for="participantCohort">Cohort:</label>
        <div class="col-sm-1">
            <input type="text" id="participantCohort" value="<?php echo (isset($the_one) ? $the_one->cohort : ''); ?>" class="form-control">
        </div>       
        
        <label class="col-sm-2 control-label  text-left" for="complete-date">LIEP Completion:</label>
        
        <div class="col-sm-2">
            <div data-date-minviewmode="months" data-date-viewmode="years" data-date-format="yyyy-mm" data-date="2012/102" id="dpYear" class="date input-group">            
                  <input type="text" readonly="" id="complete-year" value="<?php echo (isset($the_one) ? date('Y', $the_one->liep_completion) : date('Y') ); ?>" class="form-control">
                  <span class="add-on input-group-addon glyphicon glyphicon-calendar"></span>
            </div>    

            
        </div>
        <div class="col-sm-2">
            <div data-date-minviewmode="months" data-date-viewmode="months" data-date-format="mm" data-date="2012/102" id="dpMonth" class="date input-group datepicker-months">            
                  <input type="text" readonly="" id="complete-month" value="<?php echo (isset($the_one) ? date('m', $the_one->liep_completion) : date('m')); ?>" class="form-control">
                  <span class="add-on input-group-addon glyphicon glyphicon-calendar"></span>
            </div>    

            
        </div>            
        <div class="col-sm-3 pull-right">
            <input class="btn btn-default form-control" role="add-new-participant" type="button"  value="<?php
            if(isset($the_one) )
                echo 'Update';
            else 
                echo 'Save';                
            ?>">
        </div>
      </div>
      
      
        
        
    <div class="form-group footer-marginer">
        
                 
    </div>
  <?php 
        if(isset($author) )
            echo '<input id="author" type="hidden" value="'.$author.'">';
        
        if(isset($the_one) ){
            echo '<input id="action" type="hidden" value="update">';
            echo '<input id="p_id" type="hidden" value="'.$the_one->id.'">';
        }
        else
            echo '<input id="action" type="hidden" value="insert">';
  ?>
    </form>
</div>
<?php if(isset($the_one) ){ ?>
<script>
var updateJSON = {};

    updateJSON.aq=["<?php echo implode('", "', $aq_info); ?>"];
    updateJSON.cer=["<?php echo implode('", "', $cer_info); ?>"];
    updateJSON.edu=["<?php echo implode('", "', $edu_info); ?>"];
    updateJSON.skills=["<?php echo implode('", "', $skills); ?>"];
</script>
<?php } ?>
<script>
<?php
if(count($skillsDATA) > 0 ){

    echo 'var skillsJSON = [';
    $i = ( count( $skillsDATA ) - 1 );
    foreach($skillsDATA as $k=>$v){
        echo "{name: '".$v['value']."'}";
        if( $k < $i )
            echo ',';        
    }
    echo '];';
}

?>
</script>