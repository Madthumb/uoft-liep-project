‌<div class="table-responsive" id="participant-list">       
<table class="table table-bordered tablesorter" id="myTable">
    <thead>
      <tr>
          <td colspan="9">
              <input type="text" class="search col-sm-5" placeholder="* Instant Search" />
              <span class="tip">*By typing the participant name, stream name, cohort number, the following table will filtering the result automatically.</span>
          </td>
      </tr>  
      <tr>
        <th>#</th>
        <th>ID</th>
        <th>Stream</th>
        <th>Name</th>
        <th>Experience</th>
        <th>Complete</th>
        <th>Cohort</th>
        <th colspan="3" class="no-print" style="text-align: center;">Action</th>
      </tr>
    </thead>
    <tbody class="list"><?php
    
    if( isset($participant) )
    {
            $html = '';
            $i = 1;

            foreach( $participant as $key => $value ){
                $html .= '<tr id="row-'.$value->id.'" role="'.$value->active.'">';
                $html .= '<td>'.$i.'</td>';
                $html .= '<td>'.$value->p_id.'</td>';
                $html .= '<td class="streamName">'.$steams[searchForId($steams, 'id', $value->stream_id)]->name.'</td>';   
                $html .= '<td class="name">'.str_replace('|',' ',$value->full_name).'</td>';
                $html .= '<td>'.$ex_range[$value->years_of_experience].'</td>';
                $html .= '<td class="compdate">'.date('M Y', $value->liep_completion).'</td>';
                $html .= '<td class="cohort">'.$value->cohort.'</td>';
                
                
                // Action Buttons    
                $html .= '<td class="no-print"><a title="edit" href="'.config_item('app_url').'participant/edit/'.$value->id.'.html"><span class="glyphicon glyphicon-edit"></span></a></td>';
                $html .= '<td class="no-print">';
                
                if( $value->active == 'no')
                    $html .= '<a class="puser-alink" title="enable" role="'.$value->id.'"><span class="glyphicon glyphicon-eye-open"></span></a>';
                else
                    $html .= '<a class="puser-alink" title="disable" role="'.$value->id.'"><span class="glyphicon glyphicon-eye-close"></span></a>';
                
                $html .= '</td>';
                
                $html .= '<td class="no-print"><a title="save" class="save-pdf" href="'.config_item('app_url').'service.html?action=maker&bus=pdf&data='.$value->id.'"><span class="glyphicon glyphicon-save"></span></a></td>';

                $html .= '</tr>';                
                $i++;
            }
            
            echo $html;
    }    
    
    ?></tbody>
</table>
</div>