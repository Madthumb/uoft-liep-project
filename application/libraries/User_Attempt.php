<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Customized Library
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		Tony Lee<var.tonylee@gmail.com>
 * @copyright           Copyright (c) 2008 - 2013, Nonamedia, Inc.
 * @link		http://nonamedia.ca
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * Logging Class
 *
 * @subpackage	Libraries
 * @category	Logging
 * @author	Tony Lee<var.tonylee@gmail.com>
 */
class User_Attempt {

    
        public $extra_info              = array();
	protected $_date_fmt		= 'Y-m-d H:i:s';
	protected $_enabled		= TRUE;
	protected $user_agent		= array();	// check is_browser/is_robot/is_mobile			
	protected $user_ua		= array();
        
	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->_write_daily_attempt_log();
	}

	// --------------------------------------------------------------------
        
        
        public function access_function( $fn = '', $para ='' ){
            
            if(!empty($fn)){
                $this->$fn($para);
            }
            
        }

        /**
	 * Write Daily Attempt Log
	 *
	 * This function will called in constructor of this class
	 *
	 * @param	none
	 * @return	bool
	 */
	private function _write_daily_attempt_log( $fn ='daily_log' )
	{
		$MY =& get_instance();
                
		# load default user agent lib
		$MY->load->library('user_agent');
		
		# load helper fuction to get extra user device information
		$MY->load->helper('device_detection');
		$this->user_ua = browser_detection('full_assoc');
		
		
		# defin user agent	
		if ($MY->agent->is_browser())
		{
			$this->user_agent['agent']= $MY->agent->browser();			
		}
		elseif ($MY->agent->is_robot())
		{
			$this->user_agent['agent'] = $MY->agent->robot();
			if(!$this->user_agent['agent'])
				$this->user_agent['agent'] = 'unknow robot';			
		}

		# defin user device platform
		$this->user_agent['platform'] = $MY->agent->platform;
		
		# defin user device os		
		$this->user_agent['os'] = $this->user_ua['os'];
		
		# detect if the current device is mobile
                $ua_type = array(   'bot'=>'web bot', 
                                    'bro'=>'normal browser', 
                                    'bbro'=>'simple browser',
                                    'mobile'=>'handheld',
                                    'dow'=>'downloading agent',
                                    'lib'=>'http library'
                                );
		$this->user_agent['ua_type'] = $ua_type[$this->user_ua['ua_type']];
		
                
		# detect if the current device is mobile		
		$this->user_agent['ua_type'] === 'mobile' ? $this->user_agent['is_mobile'] = 'yes' : $this->user_agent['is_mobile'] = 'no';
		
                
		
                # get user geoip info                
                $geo_ip = $this->_fetch_geoip_info( get_user_ip() );                
                
                if($geo_ip){
                   
                    foreach ($geo_ip as $key => $value)                        
                        $this->user_agent[ 'user_'. $key ] = $value;
                    
                }
                else
                    return FALSE;
                
                $last_insert_id = FALSE;
                
                if( $fn == 'daily_log'){
                    # write daily log into database;            
                    $last_insert_id = $this->_save_daily_log_to_db( $this->user_agent, date('N') );
                }
                elseif( $fn == 'scan_log'){
                    # write daily log into database;            
                    $last_insert_id = $this->_save_scan_log_to_db($this->user_agent);                    
                }
                 elseif( $fn == 'use_log'){
                    # write daily log into database;            
                    $this->_update_scan_log_to_db();                    
                }
                
                return TRUE;                    
	}
        
        private function _save_scan_log_to_db($data){         

            $MY =& get_instance();            
                    
            $city = ( empty($data['user_city']) ? 'Toronto' : $data['user_city'] ); 
            
            $region_code = ( empty($data['user_region_code']) ? 'ON' : $data['user_region_code'] ); 
            
            $region_name = ( empty($data['user_region_name']) ? 'Ontario' : $data['user_region_name'] ); 
            
            $country_name = ( empty($data['user_country_name']) ? 'Canada' : $data['user_country_name'] ); 
            
            $agent = ( isset($data['agent']) ? $data['agent'] : 'Unknow' ); 
            
            $os =   ( empty($data['os']) ? 'nt' : $data['os'] );
            
            $data = array(
               'qrcode' => $this->extra_info['qrcode'],
               'v_code' => $this->extra_info['v_code'],
               'qrtype' => $this->extra_info['type'],
               'scan_time' => date('Y-m-d H:i:s'), 
               'user_ip' => get_user_ip2long( $data['user_ip']  ), 
               'user_region_code' => $region_code,
               'user_city' => $city,
               'user_region_name' => $region_name,
               'user_country' => $country_name,
               'user_la' => $data['user_latitude'] ,
               'user_lo' => $data['user_longitude'] ,
               'user_agent' => $agent,
               'user_platform' => $data['platform'],
               'user_os' => $os,
               'ua_type' => $data['ua_type'] , 
               'is_mobile' => $data['is_mobile']
            ); 
            
            $MY->db->insert('scanning', $data);  
            
            return $MY->db->insert_id(); 

        }
        private function _update_scan_log_to_db(){         


            $MY =& get_instance();     
            
            $query = $MY->db->query("SELECT `id` FROM `scanning` ORDER BY `scan_time` DESC LIMIT 1" );
            
            if ($query->num_rows() > 0){
                
                   $row = $query->row_array();
                   
                   $id = $row['id'];
                }
                
            $MY->db->where('id', $id);
            
            $MY->db->update('scanning', array('used' => 1)); 


        }
        
        private function _save_daily_log_to_db($data, $tableNum=1){         
            
            # get current url regardless to router
            $CI =& get_instance();            
            
            $agent = ( isset($data['agent']) ? $data['agent'] : 'Unknow' ); 
            
            $data = array(
               'user_ip' => get_user_ip2long( $data['user_ip']  ), 
               'user_region_code' => $data['user_region_code'] ,
               'user_region_name' => $data['user_region_name'] ,
               'user_country' => $data['user_country_name'] ,
               'user_la' => $data['user_latitude'] ,
               'user_lo' => $data['user_longitude'] ,
               'user_agent' => $agent,
               'user_platform' => $data['platform'] ,
               'user_os' => $data['os'] ,
               'ua_type' => $data['ua_type'] , 
               'is_mobile' => $data['is_mobile'] ,
               'user_landing_page' => $CI->config->site_url($CI->uri->uri_string()) ,
               'user_landing_time' => date('Y-m-d H:i:s'),
               'user_referral_page'=>  $CI->agent->referrer()
            );            
            
            $CI->db->insert('daily_log_'.$tableNum, $data);  
            
            return $CI->db->insert_id(); 
            
        }       
        
        /*
	 * _fetch_geoip_info
	 *  
        *
         * Send HTTP GET requests to: freegeoip.net/{format}/{ip_or_hostname}
         * NEW API now supports both HTTP and HTTPS.
         * Supported formats are csv, xml or json, which also support the callback query argument for JSONP.
         * The ip_or_hostname part is optional. Your own IP is searched if one is not provided.
         * 
         * @param	string  [csv | xml | json ]
         * @param       string  [ ip address ]   
	 * @return	string     
         */
        
        private function _fetch_geoip_info( $ip, $type ='json'){            
            
            $url = 'http://freegeoip.net/'.$type.'/'.$ip;
            
            try {
                
                $result = file_get_contents($url);
                
            } catch ( Exception $msg){
                
                log_message('ERROR', "geoip-conn-failed:\t\t" . $msg . '|' .$ip);
                return FALSE;
            }
   
            if(!$result){
                log_message('ERROR', "geoip-result-empty:\t\t". $url. '|' . $ip );
            }
            
            
            if($type == 'json' and !empty($result) )
                $result = json_decode ( utf8_encode($result), TRUE);
            
            
            return $result;
        }

}
// END Log Class

/* End of file User_attempt.php */
/* Location: ./application/libraries/User_attempt.php */