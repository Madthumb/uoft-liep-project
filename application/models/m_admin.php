<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of m_db_action
 *
 * @author tonylee
 */
class M_Admin extends MY_Model{
    
        
        private $_my_data = '';
        private $_my_func = '';
        public $start_date;
        public $end_date;

        public function __construct() {
                parent::__construct();

                $this->start_date = (isset( $_POST['start-date'] ) and !empty( $_POST['start-date'] )) 
                                    ? $_POST['start-date'] : '0000-00-00';

                $this->end_date = (isset( $_POST['end-date'] ) and !empty( $_POST['end-date'] )) 
                                    ?  $_POST['end-date'] : date('Y-m-d H:i:s');   

                //echo $this->db->last_query();                
        }
        
        public function call_fun($fun, $data = array()){
                    
                if(method_exists($this, $fun)){
                        try {
                                $this->_my_data = $data;
                                $this->_my_func = $fun;

                                return call_user_func_array( array($this, $this->_my_func) , $this->_my_data);

                        } catch (Exception $exc) {
                                echo $exc->getTraceAsString();
                        }
                }
                else{
                        return json_encode( array('service' => 'rest2') ) ;            
                }            
        }

        private function _participant_save(){

                $skills_id = array();

                //------skills--------------//
                foreach ($this->_my_data['data']['skills'] as $key => $value) {
                        if(trim($value) != '' ){
                                $skills_id[] = $this->_participant_option_save(array(
                                                        'name'=>'skills', 
                                                        'value' => $value,
                                                        'stream_id'=>$this->_my_data['data']['stream']
                                                    ));
                        }
                }
                /* 
                 *  REMOVE THE FOLLOWING INSTERTION BASED ON CLIENTS REQUEST Sept 5, 2014
                 *                 
                //------Additional Qualifications--------------//
                if( count($this->_my_data['data']['aq']) > 0 ){
                        foreach ($this->_my_data['data']['aq'] as $key => $value) {

                                list($year, $aq) = explode(')', $value);

                                $this->_participant_option_save(array(
                                                        'name'=>'aq', 
                                                        'value' => trim($aq),
                                                        'stream_id'=>$this->_my_data['data']['stream']
                                                    ));                    
                        }
                }
                //------Education--------------//            
                if( count($this->_my_data['data']['edu']) > 0 ){
                    foreach ($this->_my_data['data']['edu'] as $key => $value) {

                        list($year, $aq) = explode(')', $value);

                        $this->_participant_option_save(array(
                                                'name'=>'edu', 
                                                'value' => trim($aq),
                                                'stream_id'=>$this->_my_data['data']['stream']
                                            ));                    
                    }
                }

                //------Certifications / Associations--------------//
                if( count($this->_my_data['data']['cer']) > 0 ){
                        foreach ($this->_my_data['data']['cer'] as $key => $value) {

                                list($year, $aq) = explode(')', $value);

                                $this->_participant_option_save(array(
                                                        'name'=>'cer', 
                                                        'value' => trim($aq),
                                                        'stream_id'=>$this->_my_data['data']['stream']
                                                    ));                    
                        }            
                }
                 * 
                 */
                //------participants--------------//
                $this->_table_name = 'participants';

                $inertArr = array(  
                        'p_id' => trim($this->_my_data['data']['pid']), 
                        'stream_id' => $this->_my_data['data']['stream'], 
                        'full_name' => trim($this->_my_data['data']['fname']).'|'
                                      .trim($this->_my_data['data']['mname']).'|'
                                      .trim($this->_my_data['data']['lname']), 
                        'skills' => implode("|", $skills_id),                             
                        'years_of_experience' => $this->_my_data['data']['years'], 

                        'edu_info' => implode("|", $this->_my_data['data']['edu']), 
                        'aq_info' => implode("|", $this->_my_data['data']['aq']), 
                        'cer_info' => implode("|", $this->_my_data['data']['cer']),                 

                        'other_info' => $this->_my_data['data']['other'], 
                        'search_meta' => $this->_my_data['data']['search'], 
                        'liep_completion' => strtotime( $this->_my_data['data']['liepDate'] ),
                        'cohort' => trim($this->_my_data['data']['cohort']),

                        'author' => $this->_my_data['data']['author']                            
                );

                $id = NULL;
                if( isset($this->_my_data['filter']) and !empty($this->_my_data['filter']) ){
                        $id = $this->_my_data['filter'];
                }
                return $this->save( $inertArr, $id );
            
        }

        private function _translate_skills(){
                $this->_table_name = 'p-options';
                $res = array();
                foreach ($this->_my_data as $key => $value) {
                        $res[] = $this->get($value, TRUE);
                }
                return $res;
        }
        
        private function _participant_disable(){
                $this->_table_name = 'participants';
                $this->_primary_filter = 'id';
                return $this->save( array('active' => $this->_my_data['data']), $this->_my_data['filter']);
        }
        
        private function _participant_option_save($data){
            
                $this->_table_name = 'p-options';
                return $this->save( $data );
            
        }
        
        private function _participant_get_all(){
            
                $this->_table_name = 'participants';
                $this->_order_by = 'id';

                return $this->get();
        }
        private function _get_participants_name_and_id(){ 
            
                $q = $this->db->select('id, full_name, stream_id')
                                ->from('participants')
                                ->where(array('active' => 'yes'))
                                ->get()
                                ->result();
                return $q;
            
        }
        private function _get_search_tags(){
            
                if(isset( $_POST['options'] ) and $_POST['options'] !='all'){
                           $this->db->limit($_POST['options']);
                }
                
                $query = $this->db->select('stream_id, tag, count(*) AS num')
                                            ->from('search_tags_log')
                                            ->where('create_time >=', $this->start_date)
                                            ->where('create_time <=', $this->end_date)                       
                                            ->group_by("tag")
                                            ->order_by("num", "desc")
                                            ->get()
                                            ->result(); 
                return $query;
        }
        private function _participant_get_single(){
                $this->_table_name = 'participants';
                $this->_order_by = 'id';
                return $this->get($this->_my_data['id']);                        
        }

        private function _stream_get_all(){            
                $this->_table_name = 'streams';
                $this->_order_by = 'name';
                return $this->get();            
        }
        
        private function _stream_save(){            
                $this->_table_name = 'streams';
                return $this->save( array('name' =>  $this->_my_data['data']) );            
        }
        
        private function _steams_get_all(){
                $this->_table_name = 'streams';
                $this->_order_by = 'name';
                return $this->get();            
        }
        
        private function _stream_del(){
                $this->_table_name = 'streams';            
                return $this->delete( $this->_my_data['data']);
        }
        
        private function _stream_update(){
                $this->_table_name = 'streams';
                $this->_primary_filter = 'id';
                return $this->save( array('name' =>  $this->_my_data['data']), $this->_my_data['filter']);
        }
        
        private function _skills_update(){
                $this->_table_name = 'p-options';
                return $this->update_skills($this->_my_data['data'], $this->_my_data['filter']);
            
        }

        private function _skills_del(){
                $this->_table_name = 'p-options';   
                return $this->delete_all( array('name' => 'skills', 'value' =>$this->_my_data['data']) );
        }
        
        private function _skills_get_all(){
                $this->_table_name = 'p-options';
                return $this->get_skills();            
        }        
        
        private function _sum_stream(){

                $query = $this->db->select('stream_id, count(*) AS num')
                                            ->from('participants')
                                            ->group_by("stream_id")
                                            ->order_by("num", "desc")
                                            ->get()
                                            ->result();
                return $query;
        }
        
        private function _stream_views(){
                global $start, $end;

                $query = $this->db->select('option_id, count(*) AS num')
                                            ->from('action_log')
                                            ->where( array('option_name' => 'stream') )
                                            ->where('create_time >=', $this->start_date)
                                            ->where('create_time <=', $this->end_date)
                                            ->group_by("option_id")
                                            ->order_by("num", "desc")
                                            ->get()
                                            ->result();
                return $query;            
        }
        
        private function _participants_sum() {
                $sql = ' SUM(IF( `action_type` = "detail", 1, 0) ) AS "view",
                            SUM(IF( `action_type` = "share", 1, 0) ) AS "share",
                            SUM(IF( `action_type` = "print", 1, 0) ) AS "print",
                            SUM(IF( `action_type` = "download", 1, 0) ) AS "download",
                            SUM(IF( `action_type` = "search", 1, 0) ) AS "search" ';

                if(isset( $_POST['options'] )) {
                    
                        if (is_numeric($_POST['options'])) {
                            $this->db->limit($_POST['options']);
                        }
                        
                }else{
                            $this->db->limit(10);
                }
                $query = $this->db->select('option_id, '. $sql, FALSE)
                                            ->from('action_log')
                                            ->where( array('option_name' => 'participants') )
                                            ->where('create_time >=', $this->start_date)
                                            ->where('create_time <=', $this->end_date)                   
                                            ->group_by("option_id")
                                            ->get()
                                            ->result();    
                //echo $this->db->last_query();                
                return $query;                        
        }
        
}