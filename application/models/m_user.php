<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_User extends MY_Model{
    
	public function __construct() {
            parent::__construct();
            
            if(! $this->loggedin() )
                $this->logout();
        }
        
        
	public function logout ()
	{
		$this->session->sess_destroy();
                header("Location: ".$this->config->item('app_url'), TRUE, 301);
                exit();                
	}

	public function loggedin ()
	{
		return (bool) $this->session->userdata('loggedin');
	}
        
                
}