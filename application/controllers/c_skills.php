<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Description of Skills 
*
* @author tonylee
*/
class C_Skills extends MY_Admin_Class {

        public function index(){
                // Get all stream infor
                $allstream = $this->m_admin->call_fun('_stream_get_all'); 
                if( empty( $allstream ) ){

                        header("Location: streams.html", TRUE, 302 );
                        die();                
                }
            
                $data['active_menu_index'] = 2;
                $skills = $this->m_admin->call_fun('_skills_get_all');
                $data['streams'] = $this->m_admin->call_fun('_steams_get_all');

                $data['skills'] = array();
                $stack = array();

                foreach($skills as $key => $value){

                        $data['skills'][$value['stream_id']][] = $value['value'];
                }

                if(count($data['streams']) > 0)
                {   

                        foreach( $data['skills'] as $key => $value)
                        {   
                                foreach( $data['streams'] as $k => $v)
                                {              
                                        if($v->id == $key) {
                                                $data['skills'][$v->name] = $data['skills'][$key];
                                                unset($data['skills'][$key]);   
                                        }
                                }
                        }
                }
                
                $this->load->view('/components/header', $data); 
                $this->load->view('/skills/main', $data);     
                $this->load->view('/components/footer');
        }
}

/* End of file */
