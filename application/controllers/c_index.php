<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_Index extends CI_Controller {
    
        public $data;
        
        public function index(){
            
                // Load session library
                $this->load->library('session');

                // Redirect logged in user
                if(isset($this->session->userdata['loggedin']) &&  $this->session->userdata['loggedin'] > 0)
                        $this->_inner_redirect();

                // User login   
                if( isset($_POST) and !empty($_POST) and ENVIRONMENT !="development"){
                    
                        $this->data['result'] = $this->wp_user_login();
                }
                else {
                        $data = array(
                                'name'  => 'LIEP',
                                'email' => 'psxf3@hotmail.com',
                                'id'    => '1', 
                                'roles' => 'administrator',
                                'loggedin' => TRUE
                        );
                        $this->session->set_userdata($data);   
                }


                // Load login form
                $this->load->view('v_login', $this->data);

        }
        
        /*
         * Redirect to dashboard
         */
        private function _inner_redirect(){
                $uri = $this->config->item('menu');
                $uri = CIPATH. strtolower($uri['default'].".html");
                //redirect('c_home', 'location', 301); // due to WP lib, disalbe this code                    
                header("Location: ".$uri, TRUE, 301);
        }
        /*
         * Authenticate WP user by using wp default function
         */
        public function wp_user_login()
        {
                $wp_load_file = dirname(dirname(dirname(dirname(__FILE__)))).'/wp-load.php';

                if(is_file($wp_load_file))
                {
                        try{
                                require_once $wp_load_file;   
                        } catch (Exception $ex) {
                                echo $ex->getMessage();
                        }
                        // Connect to current wordpress site to auth the user
                        if(function_exists('wp_check_password') and function_exists('get_user_by') )
                        {
                                $user = get_user_by( 'login', trim($_POST['username']) );
                                if ( $user && wp_check_password( trim($_POST['password']), $user->data->user_pass, $user->ID) )
                                {                     
                                        $data = array(
                                                'name'  => $user->data->user_login,
                                                'email' => $user->data->user_email,
                                                'id'    => $user->ID, 
                                                'roles' => $user->roles[0],
                                                'loggedin' => TRUE
                                        );
                                        $this->session->set_userdata($data);   
                                        $this->_inner_redirect();    
                                        exit();
                                }
                                else
                                        return "Please check your credentials and try again.";            
                        }
                        else 
                            //Mx0132： WordPress library do now have wp_check_password and get_user_by function installed
                            return 'WordPress library error. (Mx0132)';             
                }
                else                
                    return 'WordPress library not loaded';
        }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */