<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Description of Streams 
*
* @author tonylee
*/
class C_Streams extends MY_Admin_Class {

        public function __construct() {
                parent::__construct();
        }
        public function index(){
                $data['active_menu_index'] = 1;
                $data['streams'] = $this->m_admin->call_fun('_steams_get_all');
                $this->load->view('/components/header', $data);            
                $this->load->view('/streams/main', $data);            
                $this->load->view('/components/footer');
            
        }
}

/* End of file */
