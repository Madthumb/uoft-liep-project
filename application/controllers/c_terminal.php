<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Description of test
 *
 * @author tonylee
 */
class C_Terminal extends MY_Bus_Service_Class{
    
        private $my_data = '';
        private $my_func = '';

        /**
         *  Default construct. Handle ajax calls 
         */
        public function __construct() {

                parent::__construct(); 

                if ( isset( $_REQUEST ) and !empty( $_REQUEST ) ) {

                        $this->my_data = $_REQUEST;
                        unset( $_REQUEST );

                        if ( isset( $this->my_data[ 'bus' ] ) ) {
                                $this->my_function = '_' . $this->my_data[ 'bus' ] . '_' . $this->my_data[ 'action' ];
                        }

                        // expection 
                        if( isset($_GET['action']) and isset($_GET['bus']) and $_GET['action'] == 'maker' and $_GET['bus'] == 'pdf' )
                        {
                                $this->my_data = $_GET;
                                $this->my_function = '_' . $this->my_data[ 'bus' ] . '_' . $this->my_data[ 'action' ];
                        }
                }                
        }    
        public function index(){
            
                if(method_exists($this, $this->my_function)){
                        header('Content-type: application/json');            
                        echo call_user_func_array( array($this, $this->my_function) , $this->my_data);
                }
                else{
                        echo  json_encode( array('service' => 'rest') ) ;
                }
        }     
        private function _participant_get_single(){

                $result = $this->m_admin->call_fun($this->my_function, $this->my_data);

                return $this->_return_result('int', $result);
        }

        private function _participant_save(){

                $result = $this->m_admin->call_fun($this->my_function, $this->my_data);
                return $this->_return_result('int', $result);
        }

        private function _participant_disable(){

                $result = $this->m_admin->call_fun($this->my_function, $this->my_data);
                return $this->_return_result('bool', $result);

        }
        private function _stream_save(){

                $result = $this->m_admin->call_fun($this->my_function, $this->my_data);
                return $this->_return_result('int', $result);
        }
        private function _stream_del(){
                $result = $this->m_admin->call_fun($this->my_function, $this->my_data);
                return $this->_return_result('bool', $result);
        }    

        private function _stream_update(){
                $result = $this->m_admin->call_fun($this->my_function, $this->my_data);
                return $this->_return_result('bool', $result);
        }

        private function _skills_update(){

                $result = $this->m_admin->call_fun($this->my_function, $this->my_data);
                return $this->_return_result('bool', $result);
        }

        private function _skills_del(){
                $newData = str_replace('_', ' ', $this->my_data);
                $result = $this->m_admin->call_fun($this->my_function, $newData);
                return $this->_return_result('bool', $result);
        }   

        private function _return_result($type='', $result = ''){

                if($type == 'int'){

                    if( $result >= 0 )
                        $result = array('action'=>'success', 'id'=>$result);
                    else
                        $result = array('action'=>'fail');
                }
                elseif ($type == 'bool'){

                    if( $result != FALSE )            
                        $result = array('action'=>'success');
                    else
                        $result = array('action'=>'fail');
                }
                return json_encode( $result );
        }

        private function _pdf_maker(){

                $this->load->helper(array('dompdf', 'file'));
                $this->load->helper('ams');

                $this->view_data['data'] = $this->m_admin->call_fun('_participant_get_single', array('id' => $this->my_data['data']) ); 


                $skills = array();
                $skills = array_merge($skills, $this->m_admin->call_fun('_translate_skills', explode('|', $this->view_data['data']->skills) ));

                foreach ($skills as $k => $v) {
                        $this->view_data['data']->skills = str_replace($v->id, $v->value, $this->view_data['data']->skills);
                }         

                $streams = $this->m_admin->call_fun('_steams_get_all');

                $this->view_data['data']->stream_id =$streams[searchForId($streams, 'id', $this->view_data['data']->stream_id)]->name;

                $yearEx = $this->config->item('yearsEx');
                $this->view_data['data']->years_of_experience = $yearEx[$this->view_data['data']->years_of_experience];

                $html = $this->load->view('participants/profile', $this->view_data, true);

                pdf_create($html, RESDIR.'/uploads', true);

        }    
    
}