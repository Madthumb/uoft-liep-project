<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Description of Dashboard
*
* @author tonylee
*/
class C_Dashboard extends MY_Admin_Class {


        public function index(){
            
                $data['active_menu_index'] = 'default';
                $this->load->view('/components/header', $data);
                $streams = $this->m_admin->call_fun('_steams_get_all');
                
                // Total Participants
                $this->load->view('/components/search-bar');
                
                // Total Participants
                $this->load_total_participants($streams);
                
                // Stream views counts
                $this->load_streams_view($streams);
                
                // Participants sum
                $this->load_participants_sum( $streams );
                
                // Search tags
                $this->load_searched_tags( $streams );
                
                $this->load->view('/components/footer');
            
        }
        
        public function load_total_participants($streams) {
            
                $result = $this->m_admin->call_fun('_sum_stream');
                
                if($result){
                        foreach ($result as $key => $value) {
                            foreach ($streams as $v) {
                                if( $v->id == $value->stream_id){
                                        $result[$key]->name = $v->name;
                                }
                            }
                        }
                }        
                $this->load->view('dashboard/total-participants', array('result' => $result));                
        }

        public function load_streams_view($streams) {
            
                $result = $this->m_admin->call_fun('_stream_views');
                
                if($result){
                        foreach ($result as $key => $value) {
                            foreach ($streams as $v) {
                                if( $v->id == $value->option_id){
                                        $result[$key]->name = $v->name;
                                }
                            }
                        }
                }
                $this->load->view('dashboard/streams-view', array('result' => $result));                
        }
        
        public function load_participants_sum( $streams ) {
                                    
                $user = array();
                $participants = $this->m_admin->call_fun('_get_participants_name_and_id');
                $result = array();
                //print_r( $participants );
                if( $participants ){
                    
                        foreach ($participants as $value) {
                            $user[$value->id] = str_replace('|', ' ',$value->full_name).'|'.$value->stream_id;
                        }
                        //print_r( $user );
                
                        $result = $this->m_admin->call_fun('_participants_sum');
                        // print_r( $result );
                        if($result){

                                foreach ($result as $key => $value) {    
                                        list($result[$key]->user_name, $result[$key]->stream_id) = explode('|', $user[$value->option_id]);
                                }
                                //print_r( $result );
                                foreach ($result as $key => $value) {
                                    foreach ($streams as $v) {
                                        if( $v->id == $value->stream_id){
                                                $result[$key]->stream_name = $v->name;
                                        }
                                    }
                                }  
                        }
                }
                $this->load->view('dashboard/participants-sum', array('result' => $result));                        
                
        }
        public function load_searched_tags( $streams ){
                $result = $this->m_admin->call_fun('_get_search_tags');
                if($result){
                        foreach ($streams as $k=>$v) {                    
                                foreach ($result as $key => $value) {
                                        if( $v->id == $value->stream_id){
                                                $streams[$k]->data[]= array($value->num => $value->tag);
                                        }
                                }
                        }
                }        
                $this->load->view('dashboard/tags-view', array('result' => $streams));                
        }
}

/* End of file */