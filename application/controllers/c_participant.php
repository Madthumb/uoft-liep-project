<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Description of Participant 
*
* @author tonylee
*/
class C_Participant extends MY_Admin_Class {
    
        public $data = '';
        public $id = '';
        public function __construct( )
        {
             parent::__construct();
            
            $this->data['skillsDATA'] = $this->m_admin->call_fun('_skills_get_all');             
        }
        /**
         * Load default participant controller and add views
         * 
         */
        public function index(){
            
                $allstream = $this->m_admin->call_fun('_stream_get_all');
                if( empty( $allstream ) ){                
                        header("Location: streams.html", TRUE, 302 );
                        die();                
                }

                $this->data['active_menu_index'] = 0;    

                // Form ---------------- //
                $this->load_subview_form();

                // Listing ------------- //
                $this->load_subview_list();


                $this->load_views();
        }
        /**
         * Edit selected participant by giving ID
         * @param type $id
         */
        public function edit( $id = NULL ){
                $this->id = $id;

                $this->data['active_menu_index'] = 0;    

                // Form ---------------- //
                $this->load_subview_form(); 
                $this->load_subview_edit_form();

                // Listing ------------- //
                $this->load_subview_list();


                $this->load_views();            
        }        
        /**
         * Load subview form
         */
        public function load_subview_edit_form(){
       
                $this->data['subview_data']['the_one'] = $this->m_admin->call_fun('_participant_get_single', array('id' => $this->id) ); 

                $skills = $this->m_admin->call_fun('_translate_skills', explode('|', $this->data['subview_data']['the_one']->skills));

                foreach ($this->data['subview_data']['the_one']  as $key => $value) {
                        foreach ($skills as $k => $v) {
                                $this->data['subview_data']['the_one']->skills = str_replace($v->id, $v->value, $this->data['subview_data']['the_one']->skills);
                        }
                }
        }
        /**
         * Load subview list
         */
        public function load_subview_list(){    
                // Get participants infor
                $this->data['subview_data']['participant']  = $this->m_admin->call_fun('_participant_get_all'); 

                // Get skills infor    
                $skills = array();
                foreach( $this->data['subview_data']['participant'] as $k => $v ){
                        $skills = array_merge($skills, $this->m_admin->call_fun('_translate_skills', explode('|', $v->skills) ));
                }
                // Mapping Participant Skills infor
                foreach ($this->data['subview_data']['participant']  as $key => $value) {

                        $skillset = array();
                        $skillname = array();
                        $skillset = explode('|', $value->skills);                        

                        foreach( $skillset as $kk=>$vv){

                                foreach ($skills as $k => $v) {

                                        if( $v->id == trim($vv)){
                                                $this->data['subview_data']['participant'][$key]->skillname  .= $v->value . '|';
                                        }
                                }                    
                        } 
                        $this->data['subview_data']['participant'][$key]->skills = $this->data['subview_data']['participant'][$key]->skillname;
                }
                
                $this->data['subview_data']['ex_range']  = $this->config->item('yearsEx'); 
        }   
        /**
         * Load subview form
         */
        public function load_subview_form(){
                $this->data['subview_data']['steams'] = $this->m_admin->call_fun('_steams_get_all');  
                $this->data['subview_data']['author'] = $this->session->userdata['name'];           
        }
        /**
         * Load related views
         */
        public function load_views(){
            
                $this->load->view('/components/header', $this->data);            
                $this->load->view('/participants/add_new', $this->data);            
                $this->load->view('/components/footer');            
        }
}
/* End of file */