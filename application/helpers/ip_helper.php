<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

if ( ! function_exists('get_user_ip'))
{
    function get_user_ip(){

        

        //Test if it is a shared client
        if (!empty($_SERVER['HTTP_CLIENT_IP']))
        {
            return $_SERVER['HTTP_CLIENT_IP'];
            
        }
        //Is it a proxy address
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
        {
            return $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        else{
            
            return str_replace('127.0.0.1', '99.225.123.103', $_SERVER['REMOTE_ADDR'] );
        }
        
    }
}


if ( ! function_exists('get_user_ip2long'))
{
    function get_user_ip2long($ip=''){
        
        $ip || $ip = get_user_ip();
        
        return ip2long( $ip );
        
    }
}

if ( ! function_exists('get_user_long2ip'))
{
    function get_user_long2ip($ip){
        
        return long2ip( $ip );
        
    }
}

?>
