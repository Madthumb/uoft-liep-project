<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

function pdf_create($html, $filename='', $stream=TRUE) 
{

    $filename = APPPATH.'third_party' . '/dompdf/dompdf_config.inc.php'; 
    require_once( $filename );
    $dompdf = new DOMPDF();
    
    $dompdf->load_html($html); 
    $dompdf->render();
    $pdf_data = $dompdf->output();  
    
    $uid = uniqid('profile_');    
    $pdfname = $uid.'.pdf';    
    write_file( 'resources/uploads/'.$pdfname, $pdf_data);
    
    header('Content-Type: application/pdf');
    header('Content-Disposition: attachment; filename='.$pdfname);
    header('Pragma: no-cache');
    readfile( 'resources/uploads/'.$pdfname ); 
    
    return;
    
}