<?php
function btn_edit ($uri)
{
	return anchor($uri, '<i class="icon-edit"></i>');
}

function btn_delete ($uri)
{
	return anchor($uri, '<i class="icon-remove"></i>', array(
		'onclick' => "return confirm('You are about to delete a record. This cannot be undone. Are you sure?');"
	));
}
function dis_strim($str, $delimiter = '<br>'){
    
        if(strpos($str, '|')  !== false){
            
                $temp = explode('|', $str);
                
                if( $temp ){
                        foreach ($temp as $key => $value) {
                                if(is_numeric($value)){
                                        unset( $temp[$key] );
                                }                
                        }
                        $str = implode('|', $temp);
                }
        }
        
        $str = str_replace('|', $delimiter, $str);
        //$str = str_replace('[,]', '', $str);   
		$str = preg_replace('/\[\s*\,\s*\]/', '', $str);				
		$str = preg_replace('/\,\s*\]/', '', $str);
        //$str = str_replace(',]', ']', $str);
        $str = rtrim($str, ",");

        return $str;
}

function MY_router_uri( $page ){
     $CI =& get_instance();
     return array_search( $page, $CI->router->routes );
}
function searchForId($array, $key, $value) {
           foreach ($array as $k => $v) {
               if ($v->$key == $value) {
                   return $k;
               }
           }
           return null;
}

function MY_build_restricted_navbar( $array){
	 $CI =& get_instance();
         
         $i = 0;
	 foreach ( $array as $key => $value)
	 {
               
                $id = str_replace('ams/', '', $value);
                $active = '';
                
                if( $CI->uri->segments[2] ==  $id)
                    $active = ' class="active" '; 
                    
		if( $CI->base_allow_uris['restriction'] === TRUE )
		{
			if( in_array( $value, $CI->base_allow_uris['uris'] ) )
				echo '<li'.$active.' id="'.  $id.'">' . anchor($value, $key) . '</li>';
		}
		else
			echo '<li'.$active.' id="'.  $id .'">' . anchor($value, $key) . '</li>';
                $i++;
	 }
						  	 
}
function show_result(){
        echo ' | <small>'; 
        
        echo ( isset($_POST['options']) and !is_numeric($_POST['options']) ) ? 'All Records' : 'Top 10 Records';
        
        echo '</small>';
}

function isset_value($value, $return='', $default=''){
    
        echo  (isset($value) and !empty($value) ) ? ( !empty($return) ? $return : $value) : $default;
}