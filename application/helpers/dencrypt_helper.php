<?php
/**
 * simple method to encrypt or decrypt a plain text string
 * initialization vector(IV) has to be the same when encrypting and decrypting
 * requires PHP 5 >= 5.3.0
 *
 * this is a beginners template for simple encryption decryption
 * before using this in production environments, please read about encryption
 *
 * @param string $action: can be 'encrypt' or 'decrypt'
 * @param string $string: string to encrypt or decrypt
 *
 * @return string
 */
function encrypt_decrypt($action, $string) {
    $output = false;
    
    $string = trim($string);
    
    $encrypt_method = "AES-256-CBC";
    $key = 'hgI3VtwxTu4JWOXHSmPlPjni';
    $iv = '9862315478965418';

    // hash
    $key = hash('sha256', $key);


    if( $action == 'encrypt' ) {
        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
       
    }
    else if( $action == 'decrypt' ){

        $output = $decryptedMessage = openssl_decrypt($string, $encrypt_method, $key, 0, $iv);
    }

    return $output;
}
/*
// ---- Usage: --------//
$plain_txt = "This is my plain text";
echo "Plain Text = $plain_txt\n";

$encrypted_txt = encrypt_decrypt('encrypt', $plain_txt);
echo "Encrypted Text = $encrypted_txt\n";

$decrypted_txt = encrypt_decrypt('decrypt', $encrypted_txt);
echo "Decrypted Text = $decrypted_txt\n";

if( $plain_txt === $decrypted_txt ) echo "SUCCESS";
else echo "FAILED";

echo "\n";
 * 
 * 
 */