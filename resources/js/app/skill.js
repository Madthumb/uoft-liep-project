/*!
* Copyright: Nanomedia Inc - This code is exclusively the property of Nanomedia Inc and all
*			 rights therein, including without limitation copyrights and trade
*			 secrets, are reserved by Nanomedia Inc. Any reproduction, disclosure,
*			 distribution or unauthorized use is strictly prohibited.
* Filename:	 main.js
* Purpose:	 Application Control
* Authors:	 Tony Lee <var.tonylee@gmail.com>
* Created: 	 Thu Feb 6th 11:20:59 2014
*/
define(['app/skill'],function () {
    
    $(document).on('click', 'a.edit', function(){        
        liep.cusFun.EditInput('skills',$(this));
    });

    $(document).on('click', 'a.save', function(){
        liep.cusFun.SaveInput('skills',$(this));            
    });
    

    $(document).on('click','a.del', function(){            
        liep.cusFun.DeleteInput('skills',$(this)); 
    });
    
    
});            