/*!
* Copyright: Nanomedia Inc - This code is exclusively the property of Nanomedia Inc and all
*			 rights therein, including without limitation copyrights and trade
*			 secrets, are reserved by Nanomedia Inc. Any reproduction, disclosure,
*			 distribution or unauthorized use is strictly prohibited.
* Filename:	 main.js
* Purpose:	 Application Control
* Authors:	 Tony Lee <var.tonylee@gmail.com>
* Created: 	 Sta Jan 25 10:24:48 2014
*/
define(['app/participant'],function () {

        var edit_mode = false,
            edit_mode_skill = false,
            tagHTML = function(tag, content){

            return '<button type="button" class="tags insert '+tag+' btn btn-default btn-xs"><span class="tag glyphicon glyphicon-remove"></span> <span class="tag-label"> ' + content + ' </span></button> ';

        };
        
        // -------------------------------------- //
        // ~~~~ SUBMIT ~~~~~~~~~~~~~~~~~~~~~~~~~~ //
        // -------------------------------------- //
        $('input[role="add-new-participant"]').on('click', function(){
            
            if(edit_mode){
                alert( liep.Msg.tag_edit_mode_before_save  );
                return;
            }
            
            var tip		= '',
                pid		= $('#participantID').val(),
                fname   = $('#first-name').val(),
                mname	= $('#middle-name').val(),
                lname	= $('#last-name').val(),
                other		= $('#other-info').val(),
                search	= $('#search-tag').val(),
                liepDate	= $.trim($('#complete-year').val()) +'-'+ $.trim($('#complete-month').val()),
                stream	= $("#stream-id option:selected").val(),
                author	= $("#author").val(),
                years		= $("#ep-years option:selected").val(),
                cohort	= $("#participantCohort").val();
            

            ( pid !="" )			? liep.data['pid']			= pid			: tip += liep.Msg.participant_pid;
            ( fname !="" )	? liep.data['fname']	= fname	: tip += liep.Msg.participant_fname;
            ( mname !="" )	? liep.data['mname']	= mname	: '';
            ( lname !="" )		? liep.data['lname']    = lname		: tip += liep.Msg.participant_lname;
            ( liepDate !="" )	? liep.data['liepDate']	= liepDate	: tip += liep.Msg.participant_liepDate;
            ( other !="" )		? liep.data['other']		= other		: '';
            ( stream !="" )   ? liep.data['stream']   = stream	: '';
            ( cohort =="" )	? tip += liep.Msg.cohort_empty 
                            : (			(cohort !="" && isNaN(cohort) == false ) 
									?	liep.data['cohort'] = cohort 
									:	tip += liep.Msg.cohort_isNaN 
                              );
            

            if( typeof liep.data.skills == 'undefined' || liep.data.skills.length == 0 )
                tip += liep.Msg.participant_skills;

            if( typeof liep.data.edu == 'undefined' || liep.data.edu.length == 0 )
                tip += liep.Msg.participant_edu; 

            if( typeof liep.data.aq == 'undefined' || liep.data.aq.length == 0 )
                tip += liep.Msg.participant_aq;

            if( typeof liep.data.cer == 'undefined' || liep.data.cer.length == 0 )
                tip += liep.Msg.participant_cer;
						
			if(  typeof stream == 'undefined'  )
                tip += liep.Msg.stream_empty;						

            if( tip !="" )
                alert( tip );
            else{

                liep.data['search'] =   search + '|' + 
                                        liep.data.skills.toString() + '|'+
                                        liep.data.edu.toString() + '|' +
                                        liep.data.aq.toString() + '|' +
                                        liep.data.cer.toString();



                var json = {    
								aq			:   liep.data.aq, 
                                cer		:   liep.data.cer, 
                                edu		:   liep.data.edu,
                                fname	:   liep.data['fname'],
                                liepDate:   liep.data['liepDate'],
                                lname	:   liep.data['lname'],
                                mname	:   liep.data['mname'],
                                other		:   liep.data['other'],
                                pid		:   liep.data['pid'],
                                search	:   liep.data['search'],
                                skills	:   liep.data['skills'],
                                stream	:   liep.data['stream'],
                                author	:   author,
                                years		:   years,
                                cohort	:   liep.data['cohort']
                            };
 
                // ------ Post To DB ------- //
                var p_id = '';
                if( $('#p_id').length > 0)
                    p_id = $('#p_id').val();

                var result = liep.fun.ajaxPostCall(baseLink+"service",{
                                                    'bus'      :   'participant',
                                                    'action'   :   'save',
                                                    'data'     :   json , 
                                                    'filter'   :   p_id });
				result = jQuery.parseJSON( result);
                //console.log( result);
								
                if(typeof result.action != 'undefined' && result.action == 'fail')
                    alert( liep.Msg.db_connect );
                else{

                    if(p_id == '')
                        liep.cusFun.Reset();
										
					alert( liep.Msg.record_update );
                    location.reload(); 
                }



            }
        });

        // ------------------------------------------------------ //
        // ~~~~ Customized LIEP Function ~~~~~~~~ //
        // ------------------------------------------------------ //
        liep.cusFun.groupInfo = function(token){

            var year    = $('#'+token+'-year').val() || '';
            var title   = $('#'+token).val();
            var country = $('#'+token+'-country').val() || '';
            var tag     = $('#add-'+token+'-result').html();    
            
            if(title != ''){
                
                tag += tagHTML(token+'-tag', title + ' [' + year + ', ' + country + ']' );
                $('#add-'+token+'-result').html(tag);

                $('#'+token+'-year').val('');    
                $('#'+token).val('');
                $('#'+token+'-country').val('');        

                liep.cusFun.AddTags (token, "."+token+"-tag"); 
                
           }
           else
               alert( liep.Msg.mandatory_input );
           
   
        };
        liep.cusFun.AddTags = function (tag, el){

            var temp = [];    
            liep.data[tag] = [];

            $(el).each(function(index){

               temp.push( $.trim( $(this).children('span.tag-label').html()) );
               
            }); 

            // get Skills tag
            liep.data[tag] = temp;    
        };
        liep.cusFun.Reset = function (){
                $('#participantID').val('');
                $('#first-name').val('');
                $('#middle-name').val('');
                $('#last-name').val('');
                $('#other-info').val('');
                $('#search-tag').val('');
                $('#complete-date').val('');
        };
        liep.cusFun.RemoveEL = function(token){
            $( "#add-"+token+"-result div[id^='"+token+"']" ).each(function() {
                if( $(this).is(":hidden") ){
                    liep.data[token].removeElm( $(this).children().children('span.tag-label').html() ); 
                    $(this).remove();
                }
            });   
        };
        // -------------------------------------- //
        // ~~~~ Certifications / Associations ~~~ //
        // -------------------------------------- //
        $('#cer-btn').on('click', function(){
            if(edit_mode){
              $( "#add-cer-result div[id^='cer']" ).each(function() {
                  if( $(this).is(":hidden") ){                   
                      liep.data['cer'].removeElm($(this).children().children('span.tag-label').html()); 
                      $(this).remove();
                  }
              });                   
              edit_mode = false;
            }
            liep.cusFun.groupInfo( 'cer' );
        });
        // -------------------------------------- //
        // ~~~~ Additional Qualifications ~~~~~~~ //
        // -------------------------------------- //
        $('#aq-btn').on('click', function(){
            if(edit_mode){
              $( "#add-aq-result div[id^='aq']" ).each(function() {
                  if( $(this).is(":hidden") ){                   
                      liep.data['aq'].removeElm($(this).children().children('span.tag-label').html()); 
                      $(this).remove();
                  }
              });                   
              edit_mode = false;
            }
            liep.cusFun.groupInfo( 'aq' );
        });
        // -------------------------------------- //
        // ~~~~ Education ~~~~~~~~~~~~~~~~~~~~~~~ //
        // -------------------------------------- //
        $('#edu-btn').on('click', function(){        

          if(edit_mode){
            $( "#add-edu-result div[id^='edu']" ).each(function() {
                if( $(this).is(":hidden") ){                   
                    liep.data['edu'].removeElm($(this).children().children('span.tag-label').html()); 
                    $(this).remove();
                }
            });                   
            edit_mode = false;
          }    
          liep.cusFun.groupInfo( 'edu' );
          
        });
        // -------------------------------------- //
        // ~~~~ SKills Tag ~~~~~~~~~~~~~~~~~~~~~~ //
        // -------------------------------------- //
       
        $('#addSkill-btn').on('click', function(){
            if( edit_mode ){
                $( "#add-skills-result div[id^='se']" ).each(function() {
                    if( $(this).is(":hidden") ){
                        liep.data['skills'].removeElm( $(this).children('span.tag-label').html() ); 
                        $(this).remove();
                    }
                });
                edit_mode = false;
                edit_mode_skill = false;
            }
            
            var val = $.trim( $('#addSkill').val() );
            var tag = $('#add-skills-result').html();    

            if( val != "" ){
                tag += tagHTML('skills-tag', val);        
                $('#add-skills-result').html( tag );
            }
            $('#addSkill').val('');    
            liep.cusFun.AddTags ('skills', ".skills-tag");
        });

        
        $( document ).on('click', '.tag-edit', function(){
            
            if(edit_mode === false){
                    var temp ='',
                        id = $(this).attr('target'),
                        role = $(this).attr('role'),
                        tag = $('button[role="'+role+'"]').children('span.tag-label').html();
                        //console.log( tag );
                        //console.log( tag.indexOf('[,]')  );
                    if(id == '#addSkill' ){    

                        $(id).val( tag );
                        edit_mode = true;
                        edit_mode_skill = true;
                    }

                    else{

                        var name='',year='',country='';
                        
                        if( tag.indexOf('[') == -1){
                            name = tag;
                            
                        }
                        else{
                            tag = tag.replace(/\]/g,"");               
                            tag = tag.split("[");
                            name = tag[0].replace(/\[/g,"");               
                            year = tag[1].split(',');
                            country = year[1];                       
                            year = year[0];
                       }
                       

            
                       $(id).val( $.trim(name) );
                       $(id+'-year').val( $.trim(year) );

                       $(id+'-country').parent().children('input').val( $.trim(country) );
                       $(id+'-country').parent().children('pre').html( $.trim(country) );

                       edit_mode = true;
                    }            

                    $(role).hide();
            }
            else{
                alert( liep.Msg.tag_edit_mode  );
            }
            
        });
        
        $(document).click(function(){
            if(edit_mode_skill){
                
                if( $('#addSkill').val() == '' ){
                    $('#add-skills-result div').show();
                    edit_mode = false;
                    edit_mode_skill = false;
                }
            }
            //console.log( liep.data );
        });
        
        // -------------------- //
        // ~~~~ Del Tags ~~~~~~ //
        // -------------------- //        
        $( document ).on( 'click', 'button.tags.insert', function(){ 
            var parentID = $(this).parent().attr('id');
                parentID = parentID.split("-");
                       
            if(confirm( liep.Msg.skill_tag_del ))
            { 
                $(this).remove();
                
                if(typeof liep.data[parentID[1]] != 'undefined'){
                   liep.data[parentID[1]].removeElm( $.trim($(this).children('span.tag-label').html()) );
               }
                else 
                    alert( liep.Msg.err_obj );
            }
            
        });
        $( document ).on( 'click', '.tag-del', function(){ 

            if(edit_mode){
                alert( liep.Msg.tag_edit_mode  );
                return;
            }
            var parentID = $(this).attr('target'),
                btnGroup = $(this).attr('role');
                
                parentID = parentID.split("-");
//console.log(liep.data[parentID[1]]);
               
            if(confirm( liep.Msg.skill_tag_del ))
            {
                if(typeof liep.data[parentID[1]] != 'undefined'){

                   var el = $.trim($('button[role="'+btnGroup+'"]').children('span.tag-label').html());
//console.log( "button[role="+btnGroup+"]" );
//console.log(el);

                   liep.data[parentID[1]].removeElm( el );   
                   $(btnGroup).remove();  
                }
                else 
                    alert( liep.Msg.err_obj );
                
            }

        });

        // -------------------------------------- //
        // ~~~~ Datepicker ~~~~~~~~~~~~~~~~~~ //
        // -------------------------------------- //
            if($('#dpYear').length > 0){
                $('#dpYear').datepicker({
                    format: " yyyy",
                    viewMode: "years", 
                    minViewMode: "years"
                });
                
                $('#dpMonth').datepicker({
                    format: " mm",
                    viewMode: "months"
                });                
            }
            // Close after month selected
            if (document.addEventListener) {
                
                document.addEventListener("click", function(e){
                    if($(e.target).is('.month')) $(e.target).closest('.datepicker.dropdown-menu').hide();
                }, true); 
                
                document.addEventListener("click", function(e){
                    if($(e.target).is('.year')) $(e.target).closest('.datepicker.dropdown-menu').hide();
                }, true);                 
                
            }


        // -------------------------------------- //
        // ~~~~ Change Status ~~~~~~~~~~~~~~~~~~~ //
        // -------------------------------------- //

        $('.puser-alink').on('click', function(){

            var id = 'row-'+$(this).attr('role');        
            var status = $('#'+id).attr('role');
            var json = 'yes';
            var tips = 'enable';
            var span = 'glyphicon glyphicon-eye-close';

            if(status == 'yes'){
               json = 'no';
               tips = 'disable';
               span = 'glyphicon glyphicon-eye-open';
            }

            if (confirm("Are you sure you want to "+tips+" this record?")){

                var result = liep.fun.ajaxPostCall(baseLink+"service",
                                                   {'bus':'participant',
                                                    'action':'disable',
                                                    'data':json, 
                                                    'filter' : $(this).attr('role')  });

                if(typeof result.action != 'undefined' && result.action == 'fail')
                    alert( liep.Msg.db_connect );
                else{
                    alert( liep.Msg.record_update );
                    $('#'+id).attr('role', json);
                    $(this).children('span').attr('class', span);
                }  
            }

        });



        // -------------------------------------- //
        // ~~~~ Reset Obj for Update Process ~~~~ //
        // -------------------------------------- //
        if( typeof updateJSON != 'undefined'){
            liep.data = updateJSON;
        }

        // -------------------------------------- //
        // ~~~~ Tag Action ~~~~~~~~~~~~~~~~~~~~~~ //
        // -------------------------------------- //

        $('#add-mew-link').on('click', function(){
            if(typeof baseLink !="undefined" )
                window.location.href = baseLink+"participant";
        });

        // -------------------------------------- //
        // ~~~~ Auto Complete ~~~~~~~~~~~~~~~~~~~ //
        // -------------------------------------- //
        var countries = new Bloodhound({
            datumTokenizer: function(d) { 
                return Bloodhound.tokenizers.whitespace(d.name); 
            },
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            limit: 4,
            prefetch: {
                url: 'resources/data/countries.json',
                filter: function(list) {
                    return $.map(list, function(country) { return { name: country }; });
                }
            }
        });
        countries.initialize();

        $('.countries .typeahead').typeahead(null, {
                name        : 'countries',
                displayKey  : 'name',
                source      : countries.ttAdapter()
        });


       // var skillsJSON = skillsJSON || [];
        var skillsData = new Bloodhound({
            datumTokenizer: function(d) { return Bloodhound.tokenizers.whitespace(d.name); },
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            local: skillsJSON
        });
        skillsData.initialize();

        // instantiate the typeahead UI
        $('#addSkill').typeahead(null, {
            displayKey: 'name',
            source: skillsData.ttAdapter()
        });
        
        // -------------------------------------- //
        // ~~~~ Sorting Table ~~~~~~~~~~~~~~~~~~~ //
        // -------------------------------------- //
        
        $(function(){
            var options = {
              valueNames: [ 'name','streamName', 'cohort']
            };
            var participantList = new List('participant-list', options);
            
            $("#myTable").tablesorter(); 
            
        });
                
        
});  