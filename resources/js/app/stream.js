/*!
* Copyright: Nanomedia Inc - This code is exclusively the property of Nanomedia Inc and all
*			 rights therein, including without limitation copyrights and trade
*			 secrets, are reserved by Nanomedia Inc. Any reproduction, disclosure,
*			 distribution or unauthorized use is strictly prohibited.
* Filename:	 main.js
* Purpose:	 Application Control
* Authors:	 Tony Lee <var.tonylee@gmail.com>
* Created: 	 Sta Jan 25 10:24:48 2014
*/
define(['app/stream'],function () {
       
    
    $('#addStream-btn').on('click', function(){
        
        var stream= $('#addStream').val();
        var count =  $('[id^="wraper"]').length;
        var error = false;
        $( "input[id^='stream']" ).each(function( index ) {
            if( $.trim(stream.toLowerCase())  == $.trim( $( this ).val().toLowerCase()) )
            {   error = true;
                alert( liep.Msg.no_duplicate );
            }
        }); 
        
        if(stream !="" && !error)
        {
            // ------ Post To DB ------- //
            var result = liep.fun.ajaxPostCall( "service",  {  'bus':'stream', 'action':'save', 'data':stream } );
                result = $.parseJSON( result );

            if(typeof result.action != 'undefined' && result.action == 'fail')
                alert( liep.Msg.db_connect );
            else{

                var html = $('#add-stream-result div.row').html();

                    html += '<div class="col-xs-6 stream-wrapper" id="wraper-'+count+'" role="'+result.id+'">'+
                                    '<div class="input-group">'+
                                      '<input value="'+stream+'" type="text" class="form-control" readonly="" id="stream-'+count+'">'+
                                      '<div class="input-group-btn">'+
                                        '<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">Action <span class="caret"></span></button>'+
                                        '<ul class="dropdown-menu pull-right">'+
                                          '<li><a id="edit-'+count+'" class="edit">Edit</a></li>'+
                                          '<li><a id="save-'+count+'" class="save">Save</a></li>'+
                                          '<li class="divider"></li>'+
                                          '<li><a id="del-'+count+'" class="del">Delete</a></li>'+
                                        '</ul>'+
                                      '</div>'+
                                    '</div>'+
                                  '</div>';

                $('#add-stream-result div.row').html(html);
                $('#addStream').val('');
            }
            
        } //end if(stream !="" )
    });
    

    $(document).on('click', 'a.edit', function(){        
        liep.cusFun.EditInput('stream',$(this));
    });


    $(document).on('click', 'a.save', function(){
        liep.cusFun.SaveInput('stream',$(this));            
    });
    

    $(document).on('click','a.del', function(){            
        liep.cusFun.DeleteInput('stream',$(this)); 
    });
    
});            