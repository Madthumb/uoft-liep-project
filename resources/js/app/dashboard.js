define(['app/dashboard'],function () {

        function lastNDays(n) {
                var today = new Date();
                var before = new Date();
                before.setDate(today.getDate() - n);
                var year = before.getFullYear();

                var month = before.getMonth() + 1;
                if (month < 10) {
                        month = '0' + month;
                }
                
                var day = before.getDate();
                if (day < 10) {
                        day = '0' + day;
                }

                return [year, month, day].join('-');
        }    
        
        $( document ).ready(function() {
                // -------------------------------------- //
                // ~~~~ Sorting Table ~~~~~~~ //
                // -------------------------------------- //
                var options = {
                  valueNames: [ 'username','streamname', 'view', 'share', 'print', 'search', 'download']
                };
                var participantList = new List('participant-list', options);

                //$("#myTable").tablesorter(); 
                $("#myTable").tablesorter().tablesorterPager({container: $("#pager")});
            
                // -------------------------------------- //
                // ~~~~ Load data picker ~~~~~ //
                // -------------------------------------- //                
                var nowTemp = new Date();
                var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
                
                var checkin = $('#dpd1').datepicker({   format: 'yyyy-mm-dd'})
                                                .on('changeDate', function (ev) {
                                                                if (ev.date.valueOf() > checkout.date.valueOf()) {
                                                                        var newDate = new Date(ev.date)
                                                                        newDate.setDate(newDate.getDate() + 1);
                                                                        checkout.setValue(newDate);
                                                                }
                                                                checkin.hide();
                                                                $('#dpd2')[0].focus();
                                                        })
                                                .data('datepicker');
                                            
                var checkout = $('#dpd2').datepicker({  format: 'yyyy-mm-dd'})
                                                .on('changeDate', function (ev) {
                                                            checkout.hide();
                                                        })
                                                .data('datepicker');            

            
            
        });    
});  