/*!
* Copyright: Nanomedia Inc - This code is exclusively the property of Nanomedia Inc and all
*			 rights therein, including without limitation copyrights and trade
*			 secrets, are reserved by Nanomedia Inc. Any reproduction, disclosure,
*			 distribution or unauthorized use is strictly prohibited.
* Filename:	 main.js
* Purpose:	 Application Control
* Authors:	 Tony Lee <var.tonylee@gmail.com>
* Created: 	 Sat Jan 25 10:24:48 2014
*/
requirejs.config({

    paths: { 

            jquery		:		['http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min', 'jquery-1.10.2.min'],
            jqmigrate	:		['http://code.jquery.com/jquery-migrate-1.0.0', 'lib/jquery-migrate-1.0.0'],
            bootstrap	:		['http://netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min', 'lib/bootstrap.min'],
            bdatepicker:		['lib/bootstrap-datepicker'],
            typeahead	:		['lib/typeahead'],
            tablesorter	:		['lib/jquery.tablesorter.min'], 
            tablepager	:		['lib/jquery.tablesorter.pager']
    },
    shim: {
            jquery		:	{exports: '$'},
            bootstrap  :	{deps   : ['jquery']},
            bdatepicker:	{deps   : ['jquery'], exports: 'BDP'},
            typeahead	:	{deps   : ['jquery'], exports: 'TYPEHD'},
            tablesorter	:	{deps   : ['jquery'], exports: 'TSORT'},
            tablepager	:	{deps   : ['jquery', 'tablesorter'], exports: 'TPAGER'}
    }
});

// -------------------------------------- //
// ~~~~ Global Setting ~~~~~~~//
// -------------------------------------- //
var base = window.location;

var rule = /\/edit\/.*html/g;
var newPathname  = base.pathname; 
     newPathname =  newPathname.replace( rule, ".html" );
    

var liep = {};
        liep = {
                uri:  {   home              : base.protocol + '//' + base.host,
                            currPathname  : newPathname.split('/'),
                            currLink            : base.href,
                            currQueryStr      : base.search     
                        },
                        
                fun: {},
                
                data:[]
        };
           
        var indexValue = 1;       
        for (var j=0; j<liep.uri.currPathname.length; j++) 
        {
                if (liep.uri.currPathname[j].indexOf(".html")) {
                        indexValue = j;
                }
        }
        
        
    liep.uri.currScript = liep.uri.currPathname[indexValue];       
    liep.uri.currChunk  = liep.uri.currScript.split('.')[0]; 
      
// -------------------------------------- //
// ~~~~ Global Functions ~~~~~~~~~~~~~~~~ //
// -------------------------------------- //
liep.fun.Capitalize = function(str){
    return string.charAt(0).toUpperCase() + string.slice(1);
};

liep.fun.CapitalizeAll = function(str){
    var pieces = str.split(" ");
    for ( var i = 0; i < pieces.length; i++ )
    {
        var j = pieces[i].charAt(0).toUpperCase();
        pieces[i] = j + pieces[i].substr(1);
    }
    return pieces.join(" ");
};

liep.fun.OpenInNewTab = function(url){
        var win=window.open(url, '_blank');
            win.focus();
};

liep.fun.ajaxPostCall = function(url, json){
    return $.ajax({
        type: "POST",
        url: url,
                dataType: 'json',
                data: json,
                cache: false,		
        async: false
    }).responseText;	
};

Array.prototype.removeElm = function(value) {
  //var idx = this.indexOf(value);
  var idx = jQuery.inArray( value, this );

  if (idx != -1) {
      return this.splice(idx, 1); // The second parameter is the number of elements to remove.
  }
  return false;
};

// -------------------------------------- //
// ~~~~ Customized LIEP Function ~~~~~~~~ //
// -------------------------------------- //
liep.cusFun={};

liep.cusFun.GetID = function(ref, token){

   var id = $(ref).attr('id');
       id = id.replace(token+"-","");      

       return id;
};

liep.cusFun.EditInput = function(el, ref){
    console.log(el, ref);
    
    $('input[id^="'+el+'"]').each(function( index ) {
        $(this).attr("readonly", true);
    });         

    var id = liep.cusFun.GetID(ref, 'edit');
    $('#'+el+'-'+id).attr("readonly", false);    
};

liep.cusFun.DeleteInput = function(el, ref){

    var confirmMSG = '';

    switch(el)
    {
        case 'skills':
          confirmMSG =  liep.Msg.skills_del_confirm;
          break;
        case 'stream':
          confirmMSG =  liep.Msg.stream_del_confirm;
          break;
    }
      
    
    if( $('div[id^="wraper"]').length > 1 ){

        var id = liep.cusFun.GetID(ref, 'del'); 
        var role = $('#wraper-'+id).attr('role');

         // ------ Post To DB ------- //
         if(confirm( confirmMSG ))
         {    
            var result = liep.fun.ajaxPostCall( "service",  
                                                {  'bus':el, 
                                                   'action':'del', 
                                                   'data':role
                                                });
                result = $.parseJSON( result );

            if(typeof result.action != 'undefined' && result.action == 'success')
                $('#wraper-'+id).remove(); 
            else
                alert( liep.Msg.db_connect );
         }

    }
    else
        alert( liep.Msg.del_lastone  );   

};

liep.cusFun.SaveInput = function(el, ref){

    var id = liep.cusFun.GetID(ref, 'save');

    if(!$('#'+el+'-'+id).attr("readonly"))
    {    
        $('#'+el+'-'+id).attr("readonly", true);


        var data = $('#'+el+'-'+id).val();
        var role = $('#wraper-'+id).attr('role');                   

        var error = false;

        $( "input[id^='"+el+"']" ).each(function( index ) {

           if( $(this).attr('id') != el+'-'+id )
           {
                if( $.trim(data.toLowerCase())  == $.trim( $( this ).val().toLowerCase()) )
                {   error = true;
                    alert( liep.Msg.no_duplicate );
                }
           }

        }); 

        if( data !="" && error === false )
        {
            // ------ Post To DB ------- //
            var result = liep.fun.ajaxPostCall( "service",  
                                                {  'bus'    :   el, 
                                                   'action' :   'update', 
                                                   'data'   :   data, 
                                                   'filter' :   role 
                                                 });
                result = $.parseJSON( result );

            if(typeof result.action != 'undefined' && result.action == 'fail')
                alert( liep.Msg.db_connect );
            else{
                alert( liep.Msg.record_update );
            }
        }

    }
    else
        alert( liep.Msg.input_edit );

};

liep.Msg ={
    db_connect			: 'Database connection error, please try again later.',
    err_obj					: "Object doesn't exist.",   
    record_update		: 'Record updated successfully.',
    del_lastone			: 'You must have at lease one Stream available.',
    input_edit				: 'Please select the "Edit" from the action menu first.',
    stream_del_confirm: 'By deleting this Stream, the associated participant record will no longer under any Stream. Do you still want to delte it?',  
    no_duplicate			: 'Please do not enter a duplicate stream name.',
    skill_tag_del			: 'Are you sure you want to remove this skill tag from this participant?',
    skills_del_confirm	: ' Any change to the Skills Tag(s) will affecting to all the related participant record. Do you still want to delte it?',         
    mandatory_input	: 'Please fill out the mandatory filed first',  
    participant_pid		: "- Participant ID can't be empty. \n",  
    participant_fname	: "- First name can't be empty. \n",
    participant_lname	: "- Last name ID can't be empty. \n",
    participant_liepDate	: "- LIEP completion date can't be empty. \n",
    participant_skills	: "- Skills and Expertise can't be empty. \n",
    participant_edu		: "- Education can't be empty. \n",
    participant_aq		: "- Additional Qualifications can't be empty. \n",
    participant_cer		: "- Certifications or Associations can't be empty. \n",
    cohort_empty			: "- Cohort can not be empty.\n",    
    stream_empty		: "- Stream can not be empty.\n",
    cohort_isNaN			: "- Cohort must be an integer.\n",    
    participant_nyear	: 'The "year" value is not a number.',
    participant_lyear	: 'The "year" value seems not a valid input.',
    participant_gyear	: 'The "year" seems in the future.',  
    tag_edit_mode		: 'Please only edit ONE item at each time.', 
    tag_edit_mode_before_save	: 'Please finish editing before you update the record.',
    end						: 'true'
}; 
                
                
// -------------------------------------- //
// ~~~~ Load LIEP Files ~~~~~~~~~~~~~~~~~ //
// -------------------------------------- //
require(['jquery', 'bootstrap','bdatepicker', 'typeahead', 'tablesorter','tablepager', 'app/'+liep.uri.currChunk], function($, Bootstrap, BDP, TYPEHD, TSORT, TPAGER ){});
    
    
//define(['jquery', 'bootstrap','bdatepicker', 'typeahead'], function($, Bootstrap, BDP, TYPEHD ){
//    require(['app/'+liep.uri.currChunk]);
//});    